package com.addhen.ppp.data;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.entity.InventoryEntity;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author Henry Addo
 */
public final class DataHelper {

    private DataHelper() {
        // No-op
    }

    public static List<InventoryEntity> loadInventories(final Context context, final Gson gson)
            throws IOException {
        final String jsonString = loadJSONFromAsset(context, "inventories/inventories.json");
        return transformInventoryEntityCollection(gson, jsonString);
    }

    public static List<CategoryEntity> loadCategories(final Context context, final Gson gson)
            throws IOException {
        final String jsonString = loadJSONFromAsset(context, "categories/categories.json");
        return transformCategoryEntityCollection(gson, jsonString);
    }

    /**
     * Loads json string from the asset forlder
     *
     * @param context      The calling context
     * @param jsonFileName The file name of the json string
     * @return The JSON string
     */
    private static String loadJSONFromAsset(Context context, final String jsonFileName)
            throws IOException {
        InputStream is = context.getAssets().open("json/" + jsonFileName);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        return new String(buffer, "UTF-8");
    }

    /**
     * Transform from valid json string to List of {@link InventoryEntity}.
     *
     * @param categoryListJsonResponse A json representing a collection of movies.
     * @return List of {@link InventoryEntity}.
     * @throws JsonSyntaxException if the gson string is not a valid json
     *                             structure.
     */
    public static List<CategoryEntity> transformCategoryEntityCollection(Gson gson,
            String categoryListJsonResponse) throws JsonSyntaxException {
        Type listOfCategoryEntityType = new TypeToken<List<CategoryEntity>>() {
        }.getType();
        List<CategoryEntity> categoryEntities = gson
                .fromJson(categoryListJsonResponse, listOfCategoryEntityType);
        return categoryEntities;
    }

    /**
     * Transforms from a valid json string to List of {@link InventoryEntity}.
     *
     * @param inventoryListJsonResponse A json representing a collection of movies.
     * @return List of {@link InventoryEntity}.
     * @throws JsonSyntaxException if the json string is not a valid json
     *                             structure.
     */
    public static List<InventoryEntity> transformInventoryEntityCollection(Gson gson,
            String inventoryListJsonResponse) throws JsonSyntaxException {
        Type listOfInventoryEntityType = new TypeToken<List<InventoryEntity>>() {
        }.getType();
        List<InventoryEntity> inventoryEntities = gson
                .fromJson(inventoryListJsonResponse, listOfInventoryEntityType);
        return inventoryEntities;
    }
}
