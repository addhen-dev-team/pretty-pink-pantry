package com.addhen.ppp.data.net.service;

/**
 * Created by eyedol on 6/9/16.
 */

import com.google.gson.Gson;

import com.addhen.ppp.data.DataHelper;
import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.entity.InventoryEntity;
import com.addhen.ppp.data.exception.NetworkConnectionException;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.mock.BehaviorDelegate;
import rx.Observable;
import timber.log.Timber;

/**
 * Concrete implementation of {@link RestfulService} for devel flavoured builds
 *
 * @author Henry Addo
 */
@Singleton
public class MockRestfulService implements RestfulService {

    private final Context mContext;

    private final Gson mGson;

    private final BehaviorDelegate<RestfulService> mDelegate;

    @Inject
    public MockRestfulService(Context context, Gson gson,
            BehaviorDelegate<RestfulService> delegate) {
        mContext = context;
        mGson = gson;
        mDelegate = delegate;
    }

    @Override
    public Observable<List<InventoryEntity>> getInventoryEntityList(int limit, int page) {
        List<InventoryEntity> inventoryEntities = null;
        try {
            inventoryEntities = DataHelper.loadInventories(mContext, mGson);
        } catch (Exception e) {
            Timber.wtf(new NetworkConnectionException(e), "%s", e.getMessage());
        }
        return mDelegate.returningResponse(inventoryEntities).getInventoryEntityList(limit, page);
    }

    @Override
    public Observable<List<CategoryEntity>> getCategories() {
        List<CategoryEntity> categoryEntities = null;
        try {
            categoryEntities = DataHelper.loadCategories(mContext, mGson);
        } catch (Exception e) {
            Timber.wtf(new NetworkConnectionException(e), "%s", e.getMessage());
        }
        return mDelegate.returningResponse(categoryEntities).getCategories();
    }
}
