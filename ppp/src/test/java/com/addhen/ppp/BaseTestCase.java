package com.addhen.ppp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import android.text.TextUtils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * All unit test cases have to inherit from this. This is to make it easier to
 * annotate every class with RunWith annotation
 *
 * @author Henry Addo
 */
@RunWith(JUnit4.class)
@SuppressWarnings({"PMD.AbstractClassWithoutAbstractMethod"})
public abstract class BaseTestCase {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    protected Gson mGson;

    @Before
    public void setUp() throws Exception {
        mGson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOfT,
                            JsonDeserializationContext context) throws JsonParseException {
                        final SimpleDateFormat parser = new SimpleDateFormat(
                                "yyyy-MM-dd'T'HH:mm:ss",
                                Locale.getDefault());
                        try {
                            if (TextUtils.isDigitsOnly(json.getAsString())) {
                                return new Date(json.getAsLong());
                            }
                            return new Date(parser.parse(json.getAsString()).getTime());
                        } catch (ParseException e) {
                            throw new JsonParseException(e);
                        }
                    }
                }).create();
    }
}
