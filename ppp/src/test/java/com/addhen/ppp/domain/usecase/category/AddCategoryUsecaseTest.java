package com.addhen.ppp.domain.usecase.category;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.domain.repository.CategoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class AddCategoryUsecaseTest extends BaseTestCase {

    @Mock
    private Category mMockCategory;

    @Mock
    private CategoryRepository mMockCategoryRepository;

    @Mock
    private ThreadExecutor mMockThreadExecutor;

    @Mock
    private PostExecutionThread mMockPostExecutionThread;

    private AddCategoryUsecase mAddCategoryUsecase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mAddCategoryUsecase = new AddCategoryUsecase(mMockCategoryRepository, mMockThreadExecutor,
                mMockPostExecutionThread);
    }

    @Test
    public void testShouldSuccessfullyAddCategory() {
        assertThat(mAddCategoryUsecase).isNotNull();
        mAddCategoryUsecase.setCategory(mMockCategory);
        mAddCategoryUsecase.buildUseCaseObservable();
        verify(mMockCategoryRepository).putCategory(mMockCategory);
        verifyNoMoreInteractions(mMockCategoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }

    @Test
    public void testShouldThrowRuntimeException() {
        expectedException.expect(RuntimeException.class);
        assertThat(mAddCategoryUsecase).isNotNull();
        mAddCategoryUsecase.setCategory(null);
        mAddCategoryUsecase.execute(null);
    }
}
