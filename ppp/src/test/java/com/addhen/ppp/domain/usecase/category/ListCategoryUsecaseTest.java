package com.addhen.ppp.domain.usecase.category;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.repository.CategoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class ListCategoryUsecaseTest extends BaseTestCase {

    @Mock
    private CategoryRepository mMockCategoryRepository;

    @Mock
    private ThreadExecutor mMockThreadExecutor;

    @Mock
    private PostExecutionThread mMockPostExecutionThread;

    private ListCategoryUsecase mListCategoryUsecase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mListCategoryUsecase = new ListCategoryUsecase(mMockCategoryRepository, mMockThreadExecutor,
                mMockPostExecutionThread);
    }

    @Test
    public void testShouldGetCategories() {
        assertThat(mListCategoryUsecase).isNotNull();
        mListCategoryUsecase.buildUseCaseObservable();
        verify(mMockCategoryRepository).getCategories();
        verifyNoMoreInteractions(mMockCategoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }
}
