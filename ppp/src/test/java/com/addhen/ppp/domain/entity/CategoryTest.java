package com.addhen.ppp.domain.entity;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.DomainFixture;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * @author Henry Addo
 */
public class CategoryTest extends BaseTestCase {

    @Test
    public void testShouldSetCategory() {
        final Category category = DomainFixture.getCategory();
        assertThat(category).isNotNull();
        assertThat(category.name).isEqualTo("Category");
        assertThat(category._id).isNull();
        assertThat(category.key).isEqualTo(DomainFixture.KEY);
    }
}
