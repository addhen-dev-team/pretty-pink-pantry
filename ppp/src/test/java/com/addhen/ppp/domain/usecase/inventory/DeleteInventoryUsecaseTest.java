package com.addhen.ppp.domain.usecase.inventory;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.repository.InventoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class DeleteInventoryUsecaseTest extends BaseTestCase {

    private static final String KEY = "key";

    @Mock
    private InventoryRepository mMockInventoryRepository;

    @Mock
    private ThreadExecutor mMockThreadExecutor;

    @Mock
    private PostExecutionThread mMockPostExecutionThread;

    private DeleteInventoryUsecase mDeleteInventoryUsecase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mDeleteInventoryUsecase = new DeleteInventoryUsecase(KEY, mMockInventoryRepository,
                mMockThreadExecutor, mMockPostExecutionThread);
    }

    @Test
    public void testShouldSuccessfullyDeleteInventory() {
        mDeleteInventoryUsecase.buildUseCaseObservable();
        verify(mMockInventoryRepository).deleteInventory(KEY);
        verifyNoMoreInteractions(mMockInventoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }
}
