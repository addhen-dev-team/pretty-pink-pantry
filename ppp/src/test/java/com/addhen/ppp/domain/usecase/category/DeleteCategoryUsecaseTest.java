package com.addhen.ppp.domain.usecase.category;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.repository.CategoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class DeleteCategoryUsecaseTest extends BaseTestCase {

    private static final String KEY = "key";

    @Mock
    private CategoryRepository mMockCategoryRepository;

    @Mock
    private ThreadExecutor mMockThreadExecutor;

    @Mock
    private PostExecutionThread mMockPostExecutionThread;

    private DeleteCategoryUsecase mDeleteCategoryUsecase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mDeleteCategoryUsecase = new DeleteCategoryUsecase(KEY, mMockCategoryRepository,
                mMockThreadExecutor, mMockPostExecutionThread);
    }

    @Test
    public void testShouldSuccessfullyDeleteCategory() {
        assertThat(mDeleteCategoryUsecase).isNotNull();
        mDeleteCategoryUsecase.buildUseCaseObservable();
        verify(mMockCategoryRepository).deleteCategory(KEY);
        verifyNoMoreInteractions(mMockCategoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }
}
