package com.addhen.ppp.domain.usecase.inventory;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.repository.InventoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class GetInventoryListUsecaseTest extends BaseTestCase {

    @Mock
    private InventoryRepository mInventoryRepository;

    @Mock
    private ThreadExecutor mMockThreadExecutor;

    @Mock
    private PostExecutionThread mMockPostExecutionThread;

    private GetInventoryListUsecase mGetInventoryListUsecase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mGetInventoryListUsecase = new GetInventoryListUsecase(mInventoryRepository,
                mMockThreadExecutor, mMockPostExecutionThread);
    }

    @Test
    public void testShouldGetInventoryListWithLimit10AndPageZero() {
        mGetInventoryListUsecase.buildUseCaseObservable();
        verify(mInventoryRepository).getInventoryList(10, 0);
        verifyNoMoreInteractions(mInventoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }

    @Test
    public void testShouldGetInventoryListWithLimit5AndPageOne() {
        mGetInventoryListUsecase.setPage(5, 1);
        mGetInventoryListUsecase.buildUseCaseObservable();
        verify(mInventoryRepository).getInventoryList(5, 1);
        verifyNoMoreInteractions(mInventoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }
}
