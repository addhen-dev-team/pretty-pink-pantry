package com.addhen.ppp.domain.entity;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.DomainFixture;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * @author Henry Addo
 */
public class InventoryTest extends BaseTestCase {

    @Test
    public void testShouldSetInventory() {
        Inventory model = DomainFixture.getInventory();
        assertThat(model).isNotNull();
        assertThat(model._id).isNull();
        assertThat(model.key).isEqualTo(DomainFixture.KEY);
        assertThat(model.category).isNotNull();
        assertThat(model.category._id).isNull();
        assertThat(model.category.key).isEqualTo(DomainFixture.KEY);
        assertThat(model.category.name).isEqualTo("Category");
        assertThat(model.expiryDate).isNotNull();
        assertThat(model.item).isEqualTo("Inventory");
        assertThat(model.note).isEqualTo("I bought it");
        assertThat(model.price).isWithin(2f);
        assertThat(model.quantity).isNotNull();
        assertThat(model.quantity.initial).isEqualTo(2);
        assertThat(model.quantity.remaining).isEqualTo(1);
        assertThat(model.quantity.reorder).isEqualTo(1);
        assertThat(model.quantity.unit).isEqualTo("ll");
    }
}
