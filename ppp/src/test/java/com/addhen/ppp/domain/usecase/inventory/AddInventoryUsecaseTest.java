package com.addhen.ppp.domain.usecase.inventory;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.domain.repository.InventoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class AddInventoryUsecaseTest extends BaseTestCase {

    @Mock
    private InventoryRepository mMockInventoryRepository;

    @Mock
    private ThreadExecutor mMockThreadExecutor;

    @Mock
    private PostExecutionThread mMockPostExecutionThread;

    @Mock
    private Inventory mMockInventory;

    private AddInventoryUsecase mAddInventoryUsecase;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mAddInventoryUsecase = new AddInventoryUsecase(mMockInventoryRepository,
                mMockThreadExecutor,
                mMockPostExecutionThread);
    }

    @Test
    public void testShouldSuccessfullyAddInventory() {
        mAddInventoryUsecase.setInventory(mMockInventory);
        mAddInventoryUsecase.buildUseCaseObservable();
        verify(mMockInventoryRepository).putInventory(mMockInventory);
        verifyNoMoreInteractions(mMockInventoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }

    @Test
    public void testShouldThrowRuntimeException() {
        expectedException.expect(RuntimeException.class);
        assertThat(mAddInventoryUsecase).isNotNull();
        mAddInventoryUsecase.setInventory(null);
        mAddInventoryUsecase.execute(null);
    }
}
