package com.addhen.ppp.domain.usecase.inventory;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.repository.InventoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class FetchInventoryListUsecaseTest extends BaseTestCase {

    @Mock
    private InventoryRepository mMockInventoryRepository;

    @Mock
    private ThreadExecutor mMockThreadExecutor;

    @Mock
    private PostExecutionThread mMockPostExecutionThread;

    private FetchInventoryListUsecase mFetchInventoryListUsecase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mFetchInventoryListUsecase = new FetchInventoryListUsecase(mMockInventoryRepository,
                mMockThreadExecutor, mMockPostExecutionThread);
    }

    @Test
    public void testShouldFetchInventoryListWithLimit10AndPageZero() {
        mFetchInventoryListUsecase.buildUseCaseObservable();
        verify(mMockInventoryRepository).fetchInventoryList(10, 0);
        verifyNoMoreInteractions(mMockInventoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }

    @Test
    public void testShouldFetchInventoryListWithLimit5AndPageOne() {
        mFetchInventoryListUsecase.setPage(5, 1);
        mFetchInventoryListUsecase.buildUseCaseObservable();
        verify(mMockInventoryRepository).fetchInventoryList(5, 1);
        verifyNoMoreInteractions(mMockInventoryRepository);
        verifyNoMoreInteractions(mMockPostExecutionThread);
        verifyNoMoreInteractions(mMockThreadExecutor);
    }
}
