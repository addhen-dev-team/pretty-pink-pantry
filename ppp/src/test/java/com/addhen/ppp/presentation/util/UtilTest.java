package com.addhen.ppp.presentation.util;

import com.addhen.ppp.BaseTestCase;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * @author Henry Addo
 */
public class UtilTest extends BaseTestCase {

    @Test
    public void testShouldformatDateString() {
        String date = Util.formatEDDMMMYYY("Sat, 09 Jul, 2016");
        assertThat(date).isNotNull();
        assertThat(date).isEqualTo("09/07/2016");
    }
}
