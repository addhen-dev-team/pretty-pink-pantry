package com.addhen.ppp.presentation.model.mapper;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.DomainFixture;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.presentation.model.InventoryModel;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

/**
 * Tests for InventoryModelMapper
 *
 * @author Henry Addo
 */
public class InventoryModelMapperTest extends BaseTestCase {

    private InventoryModelMapper mInventoryModelMapper;

    @Before
    public void setUp() {
        final CategoryModelMapper categoryModelMapper = new CategoryModelMapper();
        mInventoryModelMapper = new InventoryModelMapper(categoryModelMapper);
    }

    @Test
    public void testShouldMapInventoryInventoryModel() {
        InventoryModel inventoryModel = mInventoryModelMapper.map(DomainFixture.getInventory());
        assertInventoryModel(inventoryModel);
    }

    @Test
    public void testShouldMapInventoryModel() {
        Inventory inventory = DomainFixture.getInventory();
        InventoryModel inventoryModel = mInventoryModelMapper.map(inventory);
        assertInventoryModel(inventoryModel);
    }

    @Test
    public void testShouldMapInventoryListToInventoryModelList() {
        List<InventoryModel> inventoryModelList = mInventoryModelMapper
                .map(Arrays.asList(DomainFixture.getInventory()));
        assertThat(inventoryModelList).isNotNull();
        assertThat(inventoryModelList.size()).isEqualTo(1);
        assertInventoryModel(inventoryModelList.get(0));
    }

    private void assertInventoryModel(InventoryModel model) {
        assertThat(model).isNotNull();
        assertThat(model._id).isNull();
        assertThat(model.key).isEqualTo(DomainFixture.KEY);
        assertThat(model.category).isNotNull();
        assertThat(model.category._id).isNull();
        assertThat(model.category.key).isEqualTo(DomainFixture.KEY);
        assertThat(model.category.name).isEqualTo("Category");
        assertThat(model.expiryDate).isNotNull();
        assertThat(model.item).isEqualTo("Inventory");
        assertThat(model.note).isEqualTo("I bought it");
        assertThat(model.price).isWithin(2f);
        assertThat(model.quantity).isNotNull();
        assertThat(model.quantity.initial).isEqualTo(2);
        assertThat(model.quantity.remaining).isEqualTo(1);
        assertThat(model.quantity.reorder).isEqualTo(1);
        assertThat(model.quantity.unit).isEqualTo("ll");
    }
}
