package com.addhen.ppp.presentation.presenter;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.domain.usecase.inventory.GetInventoryListUsecase;
import com.addhen.ppp.presentation.model.mapper.InventoryModelMapper;
import com.addhen.ppp.presentation.view.InventoryListView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import android.content.Context;

import rx.Subscriber;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class InventoryListPresenterTest extends BaseTestCase {

    @Mock
    private GetInventoryListUsecase mMockGetInventoryListUsecase;

    @Mock
    private InventoryModelMapper mMockInventoryModelMapper;

    @Mock
    private InventoryListView mMockViewListView;

    @Mock
    private Context mMockContext;

    private InventoryListPresenter mInventoryListPresenter;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mInventoryListPresenter = new InventoryListPresenter(mMockGetInventoryListUsecase,
                mMockInventoryModelMapper);
        mInventoryListPresenter.setView(mMockViewListView);
    }

    @Test
    public void testShouldListInventoriesWithPagination() {
        given(mMockViewListView.getAppContext()).willReturn(mMockContext);
        mInventoryListPresenter.loadInventoryList(10, true);
        verify(mMockViewListView).hideRetry();
        verify(mMockViewListView).showLoading();
        verify(mMockGetInventoryListUsecase).execute(any(Subscriber.class));
    }
}
