package com.addhen.ppp.data.entity.mapper;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.data.DataFixture;
import com.addhen.ppp.domain.entity.Category;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

/**
 * @author Henry Addo
 */
public class CategoryEntityMapperTest extends BaseTestCase {

    private CategoryEntityDataMapper mCategoryEntityDataMapper;

    @Before
    public void setUp() {
        mCategoryEntityDataMapper = new CategoryEntityDataMapper();
    }

    @Test
    public void shouldMapCategoryEntityToCategory() {
        Category category = mCategoryEntityDataMapper.map(DataFixture.getCategories());
        assertCategory(category);
    }


    @Test
    public void testShouldMapInventoryListToInventoryModelList() {
        List<Category> categoryList = mCategoryEntityDataMapper
                .map(Arrays.asList(DataFixture.getCategories()));
        assertThat(categoryList).isNotNull();
        assertThat(categoryList.size()).isEqualTo(1);
        assertCategory(categoryList.get(0));
    }

    private void assertCategory(Category category) {
        assertThat(category).isNotNull();
        assertThat(category.name).isEqualTo("Category Entity");
        assertThat(category.key).isEqualTo(DataFixture.KEY);
        assertThat(category._id).isNull();
    }
}
