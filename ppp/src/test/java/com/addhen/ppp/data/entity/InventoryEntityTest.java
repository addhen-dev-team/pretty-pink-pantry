package com.addhen.ppp.data.entity;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.data.DataFixture;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * @author Henry Addo
 */
public class InventoryEntityTest extends BaseTestCase {

    @Test
    public void testShouldSetInventory() {
        InventoryEntity model = DataFixture.getInventory();
        assertThat(model).isNotNull();
        assertThat(model.key).isEqualTo(DataFixture.KEY);
        assertThat(model.category).isNotNull();
        assertThat(model.category._id).isNull();
        assertThat(model.category.key).isEqualTo(DataFixture.KEY);
        assertThat(model.category.name).isEqualTo("Category Entity");
        assertThat(model.expiryDate).isNotNull();
        assertThat(model.name).isEqualTo("Inventory Entity");
        assertThat(model.note).isEqualTo("I bought it");
        assertThat(model.price).isWithin(2f);
        assertThat(model.quantity).isNotNull();
        assertThat(model.quantity.initial).isEqualTo(2);
        assertThat(model.quantity.remaining).isEqualTo(1);
        assertThat(model.quantity.reorder).isEqualTo(1);
        assertThat(model.quantity.unit).isEqualTo("ll");
        assertThat(model._id).isNull();
    }
}
