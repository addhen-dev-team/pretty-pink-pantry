package com.addhen.ppp.data.entity;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.data.DataFixture;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * @author Henry Addo
 */
public class CategoryEntityTest extends BaseTestCase {

    @Test
    public void testShouldSetCategoryEntity() {
        final CategoryEntity category = DataFixture.getCategories();
        assertThat(category).isNotNull();
        assertThat(category.name).isEqualTo("Category Entity");
        assertThat(category.key).isEqualTo(DataFixture.KEY);
        assertThat(category._id).isNull();
    }
}
