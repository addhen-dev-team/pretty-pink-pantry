package com.addhen.ppp.data.database;

import com.google.firebase.database.DatabaseReference;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.entity.InventoryEntity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.spy;

/**
 * @author Henry Addo
 */
@SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
public class FirebaseRealTimeDatabaseTest extends BaseTestCase {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    // Allow chained method calls without throwing NPE
    @Mock(answer = RETURNS_DEEP_STUBS)
    private DatabaseReference mDatabaseReference;

    @Mock
    private CategoryEntity mMockCategoryEntity;

    @Mock
    private InventoryEntity mMockInventoryEntity;

    private FirebaseRealTimeDatabase mFirebaseRealTimeDatabase;

    private FirebaseRealTimeDatabase mSpyFirebaseRealTimeDatabase;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mFirebaseRealTimeDatabase = new FirebaseRealTimeDatabase(mDatabaseReference);
        mSpyFirebaseRealTimeDatabase = spy(mFirebaseRealTimeDatabase);
    }

    @After
    public void destroy() {
        mFirebaseRealTimeDatabase = null;
        mSpyFirebaseRealTimeDatabase = null;
    }

    @Test
    public void testShouldGetInventoryEntityList() throws InterruptedException {
        // Given
        given(mSpyFirebaseRealTimeDatabase.getInventoryEntityList(1, 1))
                .willReturn(Observable.just(Collections.singletonList(mMockInventoryEntity)));

        // When
        TestSubscriber<List<InventoryEntity>> testSubscriber = new TestSubscriber<>();
        mSpyFirebaseRealTimeDatabase.getInventoryEntityList(1, 1)
                .subscribeOn(Schedulers.immediate())
                .subscribe(testSubscriber);

        // Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertReceivedOnNext(
                Arrays.asList(Collections.singletonList(mMockInventoryEntity)));
        testSubscriber.assertCompleted();
        testSubscriber.unsubscribe();
    }

    @Test
    public void testShouldGetInventoryEntity() throws InterruptedException {
        // Given
        given(mSpyFirebaseRealTimeDatabase.getInventoryEntity("key"))
                .willReturn(Observable.just(mMockInventoryEntity));

        // When
        TestSubscriber<InventoryEntity> testSubscriber = new TestSubscriber<>();
        mSpyFirebaseRealTimeDatabase.getInventoryEntity("key")
                .subscribeOn(Schedulers.immediate())
                .subscribe(testSubscriber);

        // Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertReceivedOnNext(Collections.singletonList(mMockInventoryEntity));
        testSubscriber.assertCompleted();
        testSubscriber.unsubscribe();
    }

    @Test
    public void testShouldPutInventory() throws InterruptedException {
        // Given
        given(mSpyFirebaseRealTimeDatabase.putInventory(mMockInventoryEntity))
                .willReturn(Observable.just(null));

        // When
        TestSubscriber<Void> testSubscriber = new TestSubscriber<>();
        mSpyFirebaseRealTimeDatabase.putInventory(mMockInventoryEntity)
                .subscribeOn(Schedulers.immediate())
                .subscribe(testSubscriber);

        // Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertReceivedOnNext(Collections.singletonList(null));
        testSubscriber.assertCompleted();
        testSubscriber.unsubscribe();
    }

    @Test
    public void testShouldDeleteInventory() throws InterruptedException {
        // Given
        given(mSpyFirebaseRealTimeDatabase.deleteInventory("key")).willReturn(Observable
                .just(Boolean.TRUE));

        // When
        TestSubscriber<Boolean> testSubscriber = new TestSubscriber<>();
        mSpyFirebaseRealTimeDatabase.deleteInventory("key")
                .subscribeOn(Schedulers.immediate())
                .subscribe(testSubscriber);

        // Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertReceivedOnNext(Collections.singletonList(Boolean.TRUE));
        testSubscriber.assertCompleted();
        testSubscriber.unsubscribe();
    }

    @Test
    public void testShouldGetCategoryEntityList() throws InterruptedException {
        // Given
        given(mSpyFirebaseRealTimeDatabase.getCategoryEntityList())
                .willReturn(Observable.just(Collections.singletonList(mMockCategoryEntity)));

        // When
        TestSubscriber<List<CategoryEntity>> testSubscriber = new TestSubscriber<>();
        mSpyFirebaseRealTimeDatabase.getCategoryEntityList()
                .subscribeOn(Schedulers.immediate())
                .subscribe(testSubscriber);

        // Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertReceivedOnNext(
                Arrays.asList(Collections.singletonList(mMockCategoryEntity)));
        testSubscriber.assertCompleted();
        testSubscriber.unsubscribe();
    }

    @Test
    public void testShouldGetCategoryEntity() throws InterruptedException {
        // Given
        given(mSpyFirebaseRealTimeDatabase.getCategoryEntity("key"))
                .willReturn(Observable.just(mMockCategoryEntity));

        // When
        TestSubscriber<CategoryEntity> testSubscriber = new TestSubscriber<>();
        mSpyFirebaseRealTimeDatabase.getCategoryEntity("key")
                .subscribeOn(Schedulers.immediate())
                .subscribe(testSubscriber);

        // Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertReceivedOnNext(Collections.singletonList(mMockCategoryEntity));
        testSubscriber.assertCompleted();
        testSubscriber.unsubscribe();
    }

    @Test
    public void testShouldPutCategory() throws InterruptedException {
        // Given
        given(mSpyFirebaseRealTimeDatabase.putCategory(mMockCategoryEntity))
                .willReturn(Observable.just(null));

        // When
        TestSubscriber<Void> testSubscriber = new TestSubscriber<>();
        mSpyFirebaseRealTimeDatabase.putCategory(mMockCategoryEntity)
                .subscribeOn(Schedulers.immediate())
                .subscribe(testSubscriber);

        // Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertReceivedOnNext(Collections.singletonList(null));
        testSubscriber.assertCompleted();
        testSubscriber.unsubscribe();
    }

    @Test
    public void testShouldDeleteCategory() throws InterruptedException {
        // Given
        given(mSpyFirebaseRealTimeDatabase.deleteCategory("key")).willReturn(Observable
                .just(Boolean.TRUE));

        // When
        TestSubscriber<Boolean> testSubscriber = new TestSubscriber<>();
        mSpyFirebaseRealTimeDatabase.deleteCategory("key")
                .subscribeOn(Schedulers.immediate())
                .subscribe(testSubscriber);

        // Then
        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
        testSubscriber.assertReceivedOnNext(Collections.singletonList(Boolean.TRUE));
        testSubscriber.assertCompleted();
        testSubscriber.unsubscribe();
    }
}
