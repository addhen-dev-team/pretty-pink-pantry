package com.addhen.ppp.data.repository.category;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.data.DataFixture;
import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.entity.mapper.CategoryEntityDataMapper;
import com.addhen.ppp.data.repository.category.datasource.CategoryDataStoreFactory;
import com.addhen.ppp.data.repository.category.datasource.CategoryFirebaseDataSource;
import com.addhen.ppp.domain.entity.Category;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import rx.Observable;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

/**
 * @author Henry Addo
 */
@SuppressWarnings({"PMD.JUnitTestsShouldIncludeAssert"})
public class CategoryDataRepositoryTest extends BaseTestCase {

    private CategoryDataRepository mCategoryDataRepository;

    @Mock
    private CategoryDataStoreFactory mCategoryDataStoreFactory;

    @Mock
    private CategoryEntityDataMapper mMockCategoryEntityMapper;


    @Mock
    private CategoryFirebaseDataSource mFirebaseDataSource;

    @Mock
    private Category mMockCategory;

    @Mock
    private CategoryEntity mMockCategoryEntity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mCategoryDataRepository = new CategoryDataRepository(mCategoryDataStoreFactory,
                mMockCategoryEntityMapper);
        given(mCategoryDataStoreFactory.createFirebaseDataStore())
                .willReturn(mFirebaseDataSource);
    }

    @Test
    public void testShouldSuccessfullyAddACategory() {
        given(mFirebaseDataSource.putCategory(mMockCategoryEntity))
                .willReturn(Observable.<Void>just(null));
        given(mMockCategoryEntityMapper.unmap(mMockCategory)).willReturn(mMockCategoryEntity);

        mCategoryDataRepository.putCategory(mMockCategory);

        verify(mCategoryDataStoreFactory).createFirebaseDataStore();
        verify(mFirebaseDataSource).putCategory(mMockCategoryEntity);
    }

    @Test
    public void testShouldSuccessfullyGetCategories() {
        List<CategoryEntity> categoryEntities = Arrays.asList(DataFixture.getCategories());
        given(mFirebaseDataSource.getCategoryEntityList())
                .willReturn(Observable.<List<CategoryEntity>>just(categoryEntities));
        given(mMockCategoryEntityMapper.map(categoryEntities))
                .willReturn(Arrays.asList(mMockCategory));

        mCategoryDataRepository.getCategories();

        verify(mCategoryDataStoreFactory).createFirebaseDataStore();
        verify(mFirebaseDataSource).getCategoryEntityList();
    }

    @Test
    public void testShouldSuccessfullyGetCategory() {
        given(mFirebaseDataSource.getCategoryEntity(DataFixture.KEY))
                .willReturn(Observable.<CategoryEntity>just(DataFixture.getCategories()));
        given(mMockCategoryEntityMapper.map(DataFixture.getCategories())).willReturn(mMockCategory);

        mCategoryDataRepository.getCategory(DataFixture.KEY);
        verify(mCategoryDataStoreFactory).createFirebaseDataStore();
        verify(mFirebaseDataSource).getCategoryEntity(DataFixture.KEY);
    }

    @Test
    public void testShouldSuccessfullyUpdateCategory() {
        given(mFirebaseDataSource.putCategory(mMockCategoryEntity))
                .willReturn(Observable.<Void>just(null));
        given(mMockCategoryEntityMapper.unmap(mMockCategory)).willReturn(mMockCategoryEntity);

        mCategoryDataRepository.putCategory(mMockCategory);

        verify(mCategoryDataStoreFactory).createFirebaseDataStore();
        verify(mFirebaseDataSource).putCategory(mMockCategoryEntity);
    }

    @Test
    public void testShouldSuccessfullyDeleteCategory() {
        given(mFirebaseDataSource.deleteCategory(DataFixture.KEY))
                .willReturn(Observable.<Boolean>just(Boolean.TRUE));

        mCategoryDataRepository.deleteCategory(DataFixture.KEY);
        verify(mCategoryDataStoreFactory).createFirebaseDataStore();
        verify(mFirebaseDataSource).deleteCategory(DataFixture.KEY);
    }
}
