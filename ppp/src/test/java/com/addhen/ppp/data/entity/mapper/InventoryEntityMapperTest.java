package com.addhen.ppp.data.entity.mapper;

import com.addhen.ppp.BaseTestCase;
import com.addhen.ppp.data.DataFixture;
import com.addhen.ppp.data.entity.InventoryEntity;
import com.addhen.ppp.domain.DomainFixture;
import com.addhen.ppp.domain.entity.Inventory;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

/**
 * Tests for InventoryModelMapper
 *
 * @author Henry Addo
 */
public class InventoryEntityMapperTest extends BaseTestCase {

    private InventoryEntityDataMapper mInventoryEntityDataMapper;

    @Before
    public void setUp() {
        CategoryEntityDataMapper categoryEntityDataMapper = new CategoryEntityDataMapper();
        mInventoryEntityDataMapper = new InventoryEntityDataMapper(categoryEntityDataMapper);
    }

    @Test
    public void testShouldMapInventoryInventoryModel() {
        Inventory inventory = mInventoryEntityDataMapper.map(DataFixture.getInventory());
        assertInventory(inventory);
    }

    @Test
    public void testShouldUnMapInventoryToInventoryEntity() {
        InventoryEntity model = mInventoryEntityDataMapper.unmap(DomainFixture.getInventory());
        assertThat(model).isNotNull();
        assertThat(model._id).isNull();
        assertThat(model.key).isEqualTo(DomainFixture.KEY);
        assertThat(model.category).isNotNull();
        assertThat(model.category._id).isNull();
        assertThat(model.category.key).isEqualTo(DomainFixture.KEY);
        assertThat(model.category.name).isEqualTo("Category");
        assertThat(model.expiryDate).isNotNull();
        assertThat(model.name).isEqualTo("Inventory");
        assertThat(model.note).isEqualTo("I bought it");
        assertThat(model.price).isWithin(2f);
        assertThat(model.quantity).isNotNull();
        assertThat(model.quantity.initial).isEqualTo(2);
        assertThat(model.quantity.remaining).isEqualTo(1);
        assertThat(model.quantity.reorder).isEqualTo(1);
        assertThat(model.quantity.unit).isEqualTo("ll");
    }

    @Test
    public void testShouldMapInventoryModel() {
        InventoryEntity inventoryEntity = DataFixture.getInventory();
        Inventory inventoryModel = mInventoryEntityDataMapper.map(inventoryEntity);
        assertInventory(inventoryModel);
    }

    @Test
    public void testShouldMapInventoryListToInventoryModelList() {
        List<Inventory> inventoryEntityList = mInventoryEntityDataMapper
                .map(Arrays.asList(DataFixture.getInventory()));
        assertThat(inventoryEntityList).isNotNull();
        assertThat(inventoryEntityList.size()).isEqualTo(1);
        assertInventory(inventoryEntityList.get(0));
    }

    private void assertInventory(Inventory model) {
        assertThat(model).isNotNull();
        assertThat(model._id).isNull();
        assertThat(model.category).isNotNull();
        assertThat(model.category.key).isEqualTo(DataFixture.KEY);
        assertThat(model.category.name).isEqualTo("Category Entity");
        assertThat(model.expiryDate).isNotNull();
        assertThat(model.item).isEqualTo("Inventory Entity");
        assertThat(model.note).isEqualTo("I bought it");
        assertThat(model.price).isWithin(2f);
        assertThat(model.quantity).isNotNull();
        assertThat(model.quantity.initial).isEqualTo(2);
        assertThat(model.quantity.remaining).isEqualTo(1);
        assertThat(model.quantity.reorder).isEqualTo(1);
        assertThat(model.quantity.unit).isEqualTo("ll");
    }
}
