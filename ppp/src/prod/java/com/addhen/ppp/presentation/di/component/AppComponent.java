package com.addhen.ppp.presentation.di.component;

import com.addhen.android.raiburari.presentation.di.component.ApplicationComponent;
import com.addhen.android.raiburari.presentation.di.module.ApplicationModule;
import com.addhen.ppp.data.net.service.RestfulService;
import com.addhen.ppp.domain.repository.CategoryRepository;
import com.addhen.ppp.domain.repository.InventoryRepository;
import com.addhen.ppp.presentation.PppApplication;
import com.addhen.ppp.presentation.di.module.ProdAppModule;
import com.addhen.ppp.presentation.di.modules.AppModule;

import javax.inject.Singleton;

import dagger.Component;

import static com.addhen.ppp.presentation.di.component.DaggerAppComponent.builder;

/**
 * Devel Flavour component
 */
@Singleton
@Component(modules = {AppModule.class, ProdAppModule.class})
public interface AppComponent extends ApplicationComponent {

    RestfulService restfulService();

    InventoryRepository inventoryRepository();

    CategoryRepository categoryRepository();

    final class Initializer {

        private Initializer() {
        } // No instances.

        public static AppComponent init(PppApplication app) {
            return builder()
                    .applicationModule(new ApplicationModule(app))
                    .appModule(new AppModule())
                    .build();
        }
    }
}
