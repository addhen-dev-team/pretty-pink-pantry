package com.addhen.ppp.presentation.di.module;

import com.google.gson.Gson;

import com.addhen.ppp.BuildConfig;
import com.addhen.ppp.data.net.OkhttpInterceptor;
import com.addhen.ppp.data.net.service.RestfulService;

import android.content.Context;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Devel flavour modules
 */
@Module
public class ProdAppModule {

    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB

    private static final int HTTP_TIMEOUT = 10;

    // Http related modules
    private static OkHttpClient.Builder createOkHttpClient(Context app) {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        File cacheDir = new File(app.getApplicationContext().getCacheDir(),
                "pp-http-cache");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        client.cache(cache);
        return client;
    }

    /**
     * Provides {@link OkHttpClient} object
     *
     * @param context The calling context
     * @return The ok http client object
     */
    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Context context, HttpLoggingInterceptor loggingInterceptor) {
        final OkHttpClient.Builder okHttpClientBuilder = createOkHttpClient(
                context.getApplicationContext());
        okHttpClientBuilder.connectTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.writeTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.readTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS);
        okHttpClientBuilder.addInterceptor(new OkhttpInterceptor());
        okHttpClientBuilder.addInterceptor(loggingInterceptor);
        return okHttpClientBuilder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    RestfulService provideRestfulService(Retrofit retrofit) {
        return retrofit.create(RestfulService.class);
    }
}
