package com.addhen.ppp.presentation;

import com.addhen.ppp.presentation.model.CategoryModel;
import com.addhen.ppp.presentation.model.InventoryModel;

import java.util.Date;

/**
 * @author Henry Addo
 */
public final class PresentationFixture {

    /**
     * The KEY of the entities
     */
    public static final String KEY = "presentationKey";

    private PresentationFixture() {
        // No instantiation
    }

    public static InventoryModel getInventory() {
        return new InventoryModel(
                KEY,
                "InventoryModel",
                2f,
                new InventoryModel.QuantityModel(2, 1, 1, "ll"),
                "I bought it",
                getCategory(),
                new Date()
        );
    }

    public static CategoryModel getCategory() {
        return new CategoryModel(KEY, "Category");
    }
}
