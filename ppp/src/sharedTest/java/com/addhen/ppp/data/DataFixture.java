package com.addhen.ppp.data;

import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.entity.InventoryEntity;

import java.util.Date;

/**
 * Provides data for data entities
 *
 * @author Henry Addo
 */
public final class DataFixture {

    /**
     * The KEY of the entities
     */
    public static final String KEY = "dataKey";

    private DataFixture() {
        // No instantiation
    }

    public static InventoryEntity getInventory() {
        InventoryEntity inventoryEntity = new InventoryEntity(
                KEY,
                "Inventory Entity",
                2f,
                new InventoryEntity.Quantity(2, 1, 1, "ll"),
                "I bought it",
                getCategories(),
                new Date()
        );
        return inventoryEntity;
    }

    public static CategoryEntity getCategories() {
        return new CategoryEntity(KEY, "Category Entity");
    }
}
