package com.addhen.ppp.domain;

import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.domain.entity.Inventory;

import java.util.Date;

/**
 * @author Henry Addo
 */
public final class DomainFixture {

    /**
     * The KEY of the entities
     */
    public static final String KEY = "domainKey";

    private DomainFixture() {
        // No instantiation
    }

    public static Inventory getInventory() {
        return new Inventory(
                KEY,
                "Inventory",
                2f,
                new Inventory.Quantity(2, 1, 1, "ll"),
                "I bought it",
                getCategory(),
                new Date()
        );
    }

    public static Category getCategory() {
        return new Category(KEY, "Category");
    }
}
