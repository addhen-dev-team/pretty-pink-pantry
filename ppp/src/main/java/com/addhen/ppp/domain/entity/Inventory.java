/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.domain.entity;

import java.util.Date;

/**
 * Inventory Entity
 *
 * @author Henry Addo
 */
public class Inventory extends DomainEntity {

    public String item;

    public Quantity quantity;

    public float price;

    public String note;

    public Category category;

    public Date expiryDate;

    public Inventory(String key, String item, float price, Quantity quantity, String note,
            Category category, Date expiryDate) {
        super(key);
        this.item = item;
        this.quantity = quantity;
        this.price = price;
        this.note = note;
        this.category = category;
        this.expiryDate = (Date) expiryDate.clone();
    }

    @Override
    public String toString() {
        return "Inventory{"
                + " key='" + key + '\''
                + ", item='" + item + '\''
                + ", quantity=" + quantity
                + ", price=" + price
                + ", note='" + note + '\''
                + ", category=" + category
                + ", expiryDate=" + expiryDate
                + ", key='" + key + '\''
                + '}';
    }

    public static class Quantity {

        public int initial;

        public int remaining;

        public int reorder;

        public String unit;

        public Quantity(int initial, int remaining, int reorder, String unit) {
            this.initial = initial;
            this.remaining = remaining;
            this.reorder = reorder;
            this.unit = unit;
        }
    }
}
