package com.addhen.ppp.domain.usecase.category;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.ppp.domain.repository.CategoryRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * @author Henry Addo
 */
public class DeleteCategoryUsecase extends Usecase {

    private final CategoryRepository mCategoryRepository;

    private final String mKey;

    @Inject
    protected DeleteCategoryUsecase(String key, CategoryRepository categoryRepository,
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mCategoryRepository = categoryRepository;
        mKey = key;
    }

    @Override
    protected Observable<Boolean> buildUseCaseObservable() {
        return mCategoryRepository.deleteCategory(mKey);
    }
}
