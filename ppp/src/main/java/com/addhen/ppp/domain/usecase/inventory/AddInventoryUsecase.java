package com.addhen.ppp.domain.usecase.inventory;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.domain.repository.InventoryRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Adds an inventory to a storage
 *
 * @author Henry Addo
 */
public class AddInventoryUsecase extends Usecase {

    private final InventoryRepository mInventoryRepository;

    private Inventory mInventory;

    @Inject
    protected AddInventoryUsecase(InventoryRepository inventoryRepository,
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mInventoryRepository = inventoryRepository;
    }

    public void setInventory(Inventory inventory) {
        mInventory = inventory;
    }

    @Override
    protected Observable<Void> buildUseCaseObservable() {
        if (mInventory == null) {
            throw new RuntimeException("Inventory is null you need to call setInventory(...)");
        }
        return mInventoryRepository.putInventory(mInventory);
    }
}
