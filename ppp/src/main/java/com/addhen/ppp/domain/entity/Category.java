package com.addhen.ppp.domain.entity;

/**
 * Shopping mCategoryEntities
 *
 * @author Henry Addo
 */
public class Category extends DomainEntity {

    public String name;

    public Category() {
        super(null);
        // Default constructor for firebase
    }

    public Category(String key, String name) {
        super(key);
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{"
                + "key='" + key + '\''
                + "name='" + name + '\''
                + '}';
    }
}
