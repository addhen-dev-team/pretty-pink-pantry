package com.addhen.ppp.domain.usecase.category;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.domain.repository.CategoryRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * @author Henry Addo
 */
public class AddCategoryUsecase extends Usecase {

    private final CategoryRepository mCategoryRepository;

    private Category mCategory;

    @Inject
    protected AddCategoryUsecase(CategoryRepository categoryRepository,
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mCategoryRepository = categoryRepository;
    }

    public void setCategory(Category category) {
        mCategory = category;
    }

    @Override
    protected Observable<Void> buildUseCaseObservable() {
        if (mCategory == null) {
            throw new RuntimeException("Category is null you need to call setCategory(...)");
        }
        return mCategoryRepository.putCategory(mCategory);
    }
}
