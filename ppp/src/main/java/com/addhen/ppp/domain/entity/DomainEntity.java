package com.addhen.ppp.domain.entity;


import com.addhen.android.raiburari.domain.entity.Entity;

/**
 * @author Henry Addo
 */
public class DomainEntity extends Entity {

    /**
     * The entities ID
     */
    public String key;

    protected DomainEntity(String key) {
        this.key = key;
    }
}
