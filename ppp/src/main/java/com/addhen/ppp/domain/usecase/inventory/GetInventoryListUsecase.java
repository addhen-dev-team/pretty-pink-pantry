/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.domain.usecase.inventory;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.domain.repository.InventoryRepository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Get Inventory List Usecase
 *
 * @author Henry Addo
 */
public class GetInventoryListUsecase extends Usecase {

    private final InventoryRepository mInventoryRepository;

    private int mPage;

    private int mLimit = 10;

    @Inject
    public GetInventoryListUsecase(InventoryRepository inventoryRepository,
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mInventoryRepository = inventoryRepository;
    }

    public void setPage(int limit, int page) {
        mLimit = limit;
        mPage = page;
    }

    @Override
    protected Observable<List<Inventory>> buildUseCaseObservable() {
        return mInventoryRepository.getInventoryList(mLimit, mPage);
    }
}
