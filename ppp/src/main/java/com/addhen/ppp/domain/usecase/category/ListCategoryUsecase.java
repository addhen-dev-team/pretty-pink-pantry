package com.addhen.ppp.domain.usecase.category;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.domain.repository.CategoryRepository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * @author Henry Addo
 */
public class ListCategoryUsecase extends Usecase {

    private final CategoryRepository mCategoryRepository;

    @Inject
    public ListCategoryUsecase(CategoryRepository categoryRepository,
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mCategoryRepository = categoryRepository;
    }

    @Override
    protected Observable<List<Category>> buildUseCaseObservable() {
        return mCategoryRepository.getCategories();
    }
}
