/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.domain.repository;

import com.addhen.ppp.domain.entity.Inventory;

import java.util.List;

import rx.Observable;

/**
 * @author Henry Addo
 */
public interface InventoryRepository {

    /**
     * Get an {@link Observable} which will emit a List of {@link Inventory}.
     */
    Observable<List<Inventory>> getInventoryList(int limit, int page);

    /**
     * Get an {@link Observable} which will emit a List of {@link Inventory}.
     */
    Observable<List<Inventory>> fetchInventoryList(int limit, int page);

    /**
     * Get an {@link Observable} which will emit a {@link Inventory} by its id.
     *
     * @param key The id to retrieve user data.
     */
    Observable<Inventory> getInventory(String key);

    /**
     * Adds an {@link Inventory} to storage and then returns an {@link Observable} for all
     * subscribers to react to it.
     *
     * @param inventory The inventory to be added
     */
    Observable<Void> putInventory(Inventory inventory);

    /**
     * Deletes an {@link Inventory} from storage and then returns an {@link Observable} for
     * all subscribers to react to it.
     *
     * @param key The inventory key to used for deletion
     */
    Observable<Boolean> deleteInventory(String key);
}
