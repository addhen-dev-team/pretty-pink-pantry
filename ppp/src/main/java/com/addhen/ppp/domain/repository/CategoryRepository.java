package com.addhen.ppp.domain.repository;

import com.addhen.ppp.domain.entity.Category;

import java.util.List;

import rx.Observable;

/**
 * @author Henry Addo
 */
public interface CategoryRepository {

    /**
     * Get an {@link Observable} which will emit a List of {@link Category}.
     */
    Observable<List<Category>> getCategories();

    /**
     * Get an {@link Observable} which will emit a {@link Category}.
     *
     * @param key The entity key to use get the category
     */
    Observable<Category> getCategory(final String key);

    /**
     * Add an {@link Category} to a storage.
     *
     * @param category The entity to be added.
     */
    Observable<Void> putCategory(Category category);

    /**
     * Delete an existing {@link Category} in a storage.
     *
     * @param key The key to use to delete the entity.
     */
    Observable<Boolean> deleteCategory(String key);
}
