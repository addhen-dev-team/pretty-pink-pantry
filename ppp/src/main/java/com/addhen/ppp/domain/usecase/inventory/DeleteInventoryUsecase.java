package com.addhen.ppp.domain.usecase.inventory;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.ppp.domain.repository.InventoryRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Deletes an existing {@link com.addhen.ppp.domain.entity.Inventory} from a storage.
 *
 * @author Henry Addo
 */
public class DeleteInventoryUsecase extends Usecase {

    private final InventoryRepository mInventoryRepository;

    private final String mKey;

    @Inject
    public DeleteInventoryUsecase(String key, InventoryRepository inventoryRepository,
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mInventoryRepository = inventoryRepository;
        mKey = key;
    }

    @Override
    protected Observable<Boolean> buildUseCaseObservable() {
        return mInventoryRepository.deleteInventory(mKey);
    }
}
