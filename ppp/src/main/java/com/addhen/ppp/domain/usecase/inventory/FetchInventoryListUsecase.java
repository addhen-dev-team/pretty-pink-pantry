package com.addhen.ppp.domain.usecase.inventory;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.domain.repository.InventoryRepository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * @author Henry Addo
 */
public class FetchInventoryListUsecase extends Usecase {

    private final InventoryRepository mInventoryRepository;

    private int mPage;

    private int mLimit = 10;

    @Inject
    public FetchInventoryListUsecase(InventoryRepository inventoryRepository,
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mInventoryRepository = inventoryRepository;
    }

    public void setPage(int limit, int page) {
        mLimit = limit;
        mPage = page;
    }

    @Override
    protected Observable<List<Inventory>> buildUseCaseObservable() {
        return mInventoryRepository.fetchInventoryList(mLimit, mPage);
    }
}
