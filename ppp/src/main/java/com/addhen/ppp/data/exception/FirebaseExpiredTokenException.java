package com.addhen.ppp.data.exception;

/**
 * @author Henry Addo
 */
public class FirebaseExpiredTokenException extends Exception {

    public FirebaseExpiredTokenException() {
        super();
    }

    public FirebaseExpiredTokenException(final String message) {
        super(message);
    }

    public FirebaseExpiredTokenException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FirebaseExpiredTokenException(final Throwable cause) {
        super(cause);
    }
}
