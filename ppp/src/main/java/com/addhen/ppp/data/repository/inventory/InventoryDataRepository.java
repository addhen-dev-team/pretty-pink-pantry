/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.data.repository.inventory;

import com.addhen.ppp.data.entity.mapper.InventoryEntityDataMapper;
import com.addhen.ppp.data.repository.inventory.datasource.InventoryDataSource;
import com.addhen.ppp.data.repository.inventory.datasource.InventoryDataStoreFactory;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.domain.repository.InventoryRepository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * {@link InventoryRepository} for retrieving inventory data.
 */
public class InventoryDataRepository implements InventoryRepository {

    private final InventoryDataStoreFactory mInventoryDataStoreFactory;

    private final InventoryEntityDataMapper mInventoryEntityDataMapper;


    /**
     * Constructs a {@link InventoryRepository}.
     *
     * @param dataStoreFactory          A factory to construct different data source
     *                                  implementations.
     * @param inventoryEntityDataMapper {@link InventoryEntityDataMapper}.
     */
    @Inject
    public InventoryDataRepository(InventoryDataStoreFactory dataStoreFactory,
            InventoryEntityDataMapper inventoryEntityDataMapper) {
        mInventoryDataStoreFactory = dataStoreFactory;
        mInventoryEntityDataMapper = inventoryEntityDataMapper;
    }

    @Override
    public Observable<List<Inventory>> getInventoryList(int limit, int page) {
        final InventoryDataSource inventoryDataSource = mInventoryDataStoreFactory
                .createFirebaseDataStore();
        return inventoryDataSource.getInventoryEntityList(limit, page)
                .map(inventoryEntities -> mInventoryEntityDataMapper.map(inventoryEntities));
    }

    @Override
    public Observable<List<Inventory>> fetchInventoryList(int limit, int page) {
        final InventoryDataSource inventoryDataSource = mInventoryDataStoreFactory
                .createFirebaseDataStore();
        return inventoryDataSource.getInventoryEntityList(limit, page)
                .map(inventoryEntities -> mInventoryEntityDataMapper.map(inventoryEntities));
    }

    @Override
    public Observable<Inventory> getInventory(String key) {
        final InventoryDataSource inventoryDataSource = mInventoryDataStoreFactory
                .createFirebaseDataStore();
        return inventoryDataSource.getInventoryEntityDetails(key)
                .map(inventoryEntity -> mInventoryEntityDataMapper.map(inventoryEntity));
    }

    @Override
    public Observable<Void> putInventory(Inventory inventory) {
        final InventoryDataSource inventoryDataSource = mInventoryDataStoreFactory
                .createFirebaseDataStore();
        return inventoryDataSource.putInventory(mInventoryEntityDataMapper.unmap(inventory));
    }

    @Override
    public Observable<Boolean> deleteInventory(String key) {
        final InventoryDataSource inventoryDataSource = mInventoryDataStoreFactory
                .createFirebaseDataStore();
        return inventoryDataSource.deleteInventory(key);
    }
}
