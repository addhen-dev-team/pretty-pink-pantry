package com.addhen.ppp.data.repository.category.datasource;

import com.addhen.ppp.data.database.FirebaseRealTimeDatabase;
import com.addhen.ppp.data.entity.CategoryEntity;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * @author Henry Addo
 */
public class CategoryFirebaseDataSource implements CategoryDataSource {

    private final FirebaseRealTimeDatabase mFirebaseRealTimeDatabase;

    @Inject
    public CategoryFirebaseDataSource(FirebaseRealTimeDatabase firebaseRealTimeDatabase) {
        mFirebaseRealTimeDatabase = firebaseRealTimeDatabase;
    }

    @Override
    public Observable<List<CategoryEntity>> getCategoryEntityList() {
        return mFirebaseRealTimeDatabase.getCategoryEntityList();
    }

    @Override
    public Observable<CategoryEntity> getCategoryEntity(String categoryId) {
        return mFirebaseRealTimeDatabase.getCategoryEntity(categoryId);
    }

    @Override
    public Observable<Void> putCategory(CategoryEntity categoryEntity) {
        return mFirebaseRealTimeDatabase.putCategory(categoryEntity);
    }

    @Override
    public Observable<Boolean> deleteCategory(String key) {
        return mFirebaseRealTimeDatabase.deleteCategory(key);
    }
}
