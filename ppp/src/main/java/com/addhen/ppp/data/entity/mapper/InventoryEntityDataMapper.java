/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.data.entity.mapper;

import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.entity.InventoryEntity;
import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.domain.entity.Inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Henry Addo
 */
public class InventoryEntityDataMapper {

    private final CategoryEntityDataMapper mCategoryEntityDataMapper;

    @Inject
    public InventoryEntityDataMapper(CategoryEntityDataMapper categoryEntityDataMapper) {
        mCategoryEntityDataMapper = categoryEntityDataMapper;
    }

    /**
     * Transform a {@link InventoryEntity} into an {@link Inventory}.
     *
     * @param inventoryEntity Object to be transformed.
     * @return {@link Inventory} if valid {@link InventoryEntity} otherwise null.
     */
    public Inventory map(InventoryEntity inventoryEntity) {
        Inventory inventory = null;
        if (inventoryEntity != null) {
            inventory = new Inventory(
                    inventoryEntity.key,
                    inventoryEntity.name,
                    inventoryEntity.price,
                    map(inventoryEntity.quantity),
                    inventoryEntity.note,
                    map(inventoryEntity.category),
                    inventoryEntity.expiryDate
            );
        }
        return inventory;
    }

    /**
     * Transform a {@link Inventory} into an {@link InventoryEntity}.
     *
     * @param inventory Object to be transformed.
     * @return {@link InventoryEntity} if valid {@link Inventory} otherwise null.
     */
    public InventoryEntity unmap(Inventory inventory) {
        InventoryEntity inventoryEntity = null;
        if (inventory != null) {
            inventoryEntity = new InventoryEntity(
                    inventory.key,
                    inventory.item,
                    inventory.price,
                    unmap(inventory.quantity),
                    inventory.note,
                    unmap(inventory.category),
                    inventory.expiryDate
            );
        }
        return inventoryEntity;
    }

    /**
     * Transform a List of {@link InventoryEntity} into a Collection of {@link Inventory}.
     *
     * @param inventoryEntityCollection Object Collection to be transformed.
     * @return {@link Inventory} if valid {@link InventoryEntity} otherwise null.
     */
    public List<Inventory> map(Collection<InventoryEntity> inventoryEntityCollection) {
        List<Inventory> inventoryList = new ArrayList<>();
        Inventory inventory;
        for (InventoryEntity inventoryEntity : inventoryEntityCollection) {
            inventory = map(inventoryEntity);
            if (inventory != null) {
                inventoryList.add(inventory);
            }
        }
        return inventoryList;
    }

    private Category map(CategoryEntity categoryEntity) {
        return mCategoryEntityDataMapper.map(categoryEntity);
    }

    private Inventory.Quantity map(InventoryEntity.Quantity quantity) {
        return new Inventory.Quantity(
                quantity.initial,
                quantity.remaining,
                quantity.reorder,
                quantity.unit
        );
    }

    private CategoryEntity unmap(Category category) {
        return mCategoryEntityDataMapper.unmap(category);
    }

    private InventoryEntity.Quantity unmap(Inventory.Quantity quantity) {
        return new InventoryEntity.Quantity(
                quantity.initial,
                quantity.remaining,
                quantity.reorder,
                quantity.unit
        );
    }
}
