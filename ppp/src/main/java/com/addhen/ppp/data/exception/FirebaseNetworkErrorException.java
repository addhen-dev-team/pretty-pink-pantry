package com.addhen.ppp.data.exception;

/**
 * @author Henry Addo
 */
public class FirebaseNetworkErrorException extends Exception {

    public FirebaseNetworkErrorException() {
        super();
    }

    public FirebaseNetworkErrorException(final String message) {
        super(message);
    }

    public FirebaseNetworkErrorException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FirebaseNetworkErrorException(final Throwable cause) {
        super(cause);
    }
}
