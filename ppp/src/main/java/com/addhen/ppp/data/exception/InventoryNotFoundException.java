/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.data.exception;

/**
 * Exception throw by the application when an Inventory search can't return a valid result.
 *
 * @author Henry Addo
 */
public class InventoryNotFoundException extends Exception {

    public InventoryNotFoundException() {
        super();
    }

    public InventoryNotFoundException(final String message) {
        super(message);
    }

    public InventoryNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InventoryNotFoundException(final Throwable cause) {
        super(cause);
    }
}
