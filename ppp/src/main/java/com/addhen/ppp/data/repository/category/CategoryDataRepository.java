package com.addhen.ppp.data.repository.category;

import com.addhen.ppp.data.entity.mapper.CategoryEntityDataMapper;
import com.addhen.ppp.data.repository.category.datasource.CategoryDataSource;
import com.addhen.ppp.data.repository.category.datasource.CategoryDataStoreFactory;
import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.domain.repository.CategoryRepository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * @author Henry Addo
 */
public class CategoryDataRepository implements CategoryRepository {

    private final CategoryDataStoreFactory mCategoryDataStoreFactory;

    private final CategoryEntityDataMapper mCategoryEntityDataMapper;


    /**
     * Constructs a {@link CategoryRepository}.
     *
     * @param dataStoreFactory         A factory to construct different data source
     *                                 implementations.
     * @param categoryEntityDataMapper {@link CategoryEntityDataMapper}.
     */
    @Inject
    public CategoryDataRepository(CategoryDataStoreFactory dataStoreFactory,
            CategoryEntityDataMapper categoryEntityDataMapper) {
        mCategoryDataStoreFactory = dataStoreFactory;
        mCategoryEntityDataMapper = categoryEntityDataMapper;
    }

    @Override
    public Observable<List<Category>> getCategories() {
        final CategoryDataSource categoryDataSource = mCategoryDataStoreFactory
                .createFirebaseDataStore();
        return categoryDataSource.getCategoryEntityList()
                .map(categoryEntities -> mCategoryEntityDataMapper.map(categoryEntities));
    }

    @Override
    public Observable<Category> getCategory(String key) {
        final CategoryDataSource categoryDataSource = mCategoryDataStoreFactory
                .createFirebaseDataStore();
        return categoryDataSource.getCategoryEntity(key)
                .map(categoryEntity -> mCategoryEntityDataMapper.map(categoryEntity));
    }

    @Override
    public Observable<Void> putCategory(Category category) {
        final CategoryDataSource categoryDataSource = mCategoryDataStoreFactory
                .createFirebaseDataStore();
        return categoryDataSource.putCategory(mCategoryEntityDataMapper.unmap(category));
    }

    @Override
    public Observable<Boolean> deleteCategory(String key) {
        final CategoryDataSource categoryDataSource = mCategoryDataStoreFactory
                .createFirebaseDataStore();
        return categoryDataSource.deleteCategory(key);
    }
}
