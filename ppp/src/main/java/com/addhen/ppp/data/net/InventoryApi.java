package com.addhen.ppp.data.net;

import com.addhen.ppp.data.entity.InventoryEntity;
import com.addhen.ppp.data.net.service.RestfulService;

import java.util.List;

import rx.Observable;

/**
 * Inventory related API calls
 *
 * @author Henry Addo
 */
public class InventoryApi {

    private final RestfulService mRestfulApi;

    /**
     * Constructor of the class
     *
     * @param restfulApi {@link RestfulService}.
     */
    public InventoryApi(RestfulService restfulApi) {
        if (restfulApi == null) {
            throw new IllegalArgumentException("The constructor parameters cannot be null!!!");
        }
        mRestfulApi = restfulApi;
    }

    public Observable<List<InventoryEntity>> getInventoryList(int limit, int page) {
        return mRestfulApi.getInventoryEntityList(limit, page);
    }
}