package com.addhen.ppp.data.exception;

/**
 * @author Henry Addo
 */
public class FirebasePermissionDeniedException extends Exception {

    public FirebasePermissionDeniedException() {
        super();
    }

    public FirebasePermissionDeniedException(final String message) {
        super(message);
    }

    public FirebasePermissionDeniedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FirebasePermissionDeniedException(final Throwable cause) {
        super(cause);
    }
}
