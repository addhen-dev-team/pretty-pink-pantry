/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.data.repository.inventory.datasource;

import com.addhen.ppp.data.entity.InventoryEntity;

import java.util.List;

import rx.Observable;

/**
 * Inventory Entity
 *
 * @author Henry Addo
 */
public interface InventoryDataSource {

    /**
     * Get an {@link Observable} which will emit a List of {@link InventoryEntity}.
     */
    Observable<List<InventoryEntity>> getInventoryEntityList(int limit, int page);

    /**
     * Get an {@link Observable} which will emit a {@link InventoryEntity} by its id.
     *
     * @param key The key to retrieve inventory data.
     */
    Observable<InventoryEntity> getInventoryEntityDetails(String key);

    /**
     * Adds an {@link InventoryEntity} to storage and then returns an {@link Observable} for all
     * subscribers to react to it.
     *
     * @param inventoryEntity The inventory to be added
     */
    Observable<Void> putInventory(InventoryEntity inventoryEntity);

    /**
     * Deletes an {@link InventoryEntity} from storage and then returns an {@link Observable} for
     * all subscribers to react to it.
     *
     * @param key The inventory key to use for deletion
     */
    Observable<Boolean> deleteInventory(String key);
}
