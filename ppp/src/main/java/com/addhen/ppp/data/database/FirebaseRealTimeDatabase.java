package com.addhen.ppp.data.database;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.entity.InventoryEntity;

import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.subscriptions.Subscriptions;

/**
 * @author Henry Addo
 */
public class FirebaseRealTimeDatabase {

    private static final String FIREBASE_CHILD_INVENTORIES = "inventories";

    private static final String FIREBASE_CHILD_CATEGORIES = "categories";

    private final DatabaseReference mDatabaseReference;

    @Inject
    public FirebaseRealTimeDatabase(DatabaseReference databaseReference) {
        mDatabaseReference = databaseReference;
    }

    /**
     * Get an {@link rx.Observable} which will emit a List of {@link InventoryEntity}.
     */
    public Observable<List<InventoryEntity>> getInventoryEntityList(int limit, int page) {
        final Query inventoryEntityReference = mDatabaseReference
                .child(FIREBASE_CHILD_INVENTORIES)
                .orderByKey()
                .limitToLast(limit);
        return observeValueEvent(inventoryEntityReference)
                .flatMap(dataSnapshot -> {
                    List<InventoryEntity> inventoryEntities = new ArrayList<>();
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                        inventoryEntities
                                .add(childDataSnapshot.getValue(InventoryEntity.class));
                    }
                    return Observable.just(inventoryEntities);
                });
    }

    /**
     * Gets an {@link rx.Observable} which will emit a single {@link InventoryEntity}.
     */
    public Observable<InventoryEntity> getInventoryEntity(String key) {
        return observeValueEvent(mDatabaseReference.child(FIREBASE_CHILD_INVENTORIES).child(key))
                .flatMap(dataSnapshot -> {
                    return Observable.just(dataSnapshot.getValue(InventoryEntity.class));
                });
    }

    public Observable<Void> putInventory(InventoryEntity inventoryEntity) {
        if (!TextUtils.isEmpty(inventoryEntity.key)) {
            return updateInventory(inventoryEntity);
        }
        final DatabaseReference ref = mDatabaseReference.child(FIREBASE_CHILD_INVENTORIES)
                .push();
        inventoryEntity.key = ref.getKey();
        return observeSetValuePush(ref, inventoryEntity);
    }

    public Observable<Boolean> deleteInventory(String key) {
        final DatabaseReference ref = mDatabaseReference.child(FIREBASE_CHILD_INVENTORIES)
                .child(key);
        return observeValueEvent(ref)
                .flatMap(firebaseChildEvent -> {
                    ref.removeValue();
                    return Observable.just(Boolean.TRUE);
                });
    }

    /**
     * Get an {@link Observable} which will emit a List of {@link CategoryEntity}.
     */
    public Observable<List<CategoryEntity>> getCategoryEntityList() {
        return observeValueEvent(mDatabaseReference.child(FIREBASE_CHILD_CATEGORIES))
                .flatMap(dataSnapshot -> {
                    List<CategoryEntity> categoryEntities = new ArrayList<>();
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                        categoryEntities
                                .add(childDataSnapshot.getValue(CategoryEntity.class));
                    }
                    return Observable.just(categoryEntities);
                });
    }

    /**
     * Gets an {@link rx.Observable} which will emit a single {@link InventoryEntity}.
     */
    public Observable<CategoryEntity> getCategoryEntity(String key) {
        final DatabaseReference ref = mDatabaseReference.child(FIREBASE_CHILD_CATEGORIES)
                .child(key);
        return observeValueEvent(ref)
                .flatMap(dataSnapshot -> {
                    ref.removeValue();
                    return Observable.just(dataSnapshot.getValue(CategoryEntity.class));
                });
    }


    public Observable<Void> putCategory(CategoryEntity categoryEntity) {
        if (!TextUtils.isEmpty(categoryEntity.key)) {
            return updateCategory(categoryEntity);
        }
        final DatabaseReference ref = mDatabaseReference.child(FIREBASE_CHILD_CATEGORIES)
                .push();
        categoryEntity.key = ref.getKey();
        return observeSetValuePush(ref, categoryEntity);
    }

    public Observable<Boolean> deleteCategory(String key) {
        final DatabaseReference ref = mDatabaseReference.child(FIREBASE_CHILD_CATEGORIES)
                .child(key);
        return observeValueEvent(ref)
                .flatMap(firebaseChildEvent -> {
                    ref.removeValue();
                    return Observable.just(Boolean.TRUE);
                });
    }

    private Observable<Void> updateInventory(InventoryEntity inventoryEntity) {
        final DatabaseReference ref = mDatabaseReference.child(FIREBASE_CHILD_INVENTORIES)
                .child(inventoryEntity.key);
        return observeSetValuePush(ref, inventoryEntity);
    }

    private Observable<Void> updateCategory(CategoryEntity categoryEntity) {
        final DatabaseReference ref = mDatabaseReference.child(FIREBASE_CHILD_CATEGORIES)
                .child(categoryEntity.getKey());
        return observeSetValuePush(ref,
                categoryEntity);
    }


    /**
     * This methods observes data saving with push in order to generate the key
     * automatically according to Firebase hashing key rules.
     *
     * @param firebaseRef {@link Query} this is reference of a Firebase Query
     * @param entity      {@link Object} whatever object we want to save
     * @return an {@link rx.Observable} of the generated key after
     * the object persistence
     */
    @VisibleForTesting
    protected Observable<Void> observeSetValuePush(final DatabaseReference firebaseRef,
            final Object entity) {
        return Observable.create(subscriber -> {
            firebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    subscriber.onNext(null);
                    subscriber.onCompleted();
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    FirebaseDatabaseErrorFactory.create(subscriber, error);
                }
            });
            firebaseRef.setValue(entity);
        });
    }

    /**
     * This methods observes a firebase query and returns back
     * an Observable of the {@link DataSnapshot}
     * when the firebase client uses a {@link ValueEventListener}
     *
     * @param firebaseRef {@link Query} this is reference of a Firebase Query
     * @return an {@link rx.Observable} of datasnapshot to use
     */
    @VisibleForTesting
    protected Observable<DataSnapshot> observeValueEvent(final Query firebaseRef) {
        return Observable.create(subscriber -> {
            final ValueEventListener listener = firebaseRef.addValueEventListener(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            subscriber.onNext(dataSnapshot);
                            subscriber.onCompleted();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            FirebaseDatabaseErrorFactory.create(subscriber, databaseError);
                        }
                    });
            // When the subscription is cancelled, remove the listener
            subscriber.add(Subscriptions.create(() -> {
                firebaseRef.removeEventListener(listener);
            }));
        });
    }
}
