package com.addhen.ppp.data.net.service;


import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.entity.InventoryEntity;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * FindReels Restful interface
 *
 * @author Henry Addo
 */
public interface RestfulService {

    /**
     * Retrieves an {@link Observable} which will emit a List of {@link InventoryEntity}.
     */
    @GET("mInventoryEntities/")
    Observable<List<InventoryEntity>> getInventoryEntityList(@Query("limit") int limit,
            @Query("page") int page);

    /**
     * Retrieves an {@link Observable} which will emit a {@link CategoryEntity}.
     */
    @GET("mCategoryEntities/")
    Observable<List<CategoryEntity>> getCategories();
}
