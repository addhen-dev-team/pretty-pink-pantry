package com.addhen.ppp.data.repository.category.datasource;

import com.addhen.ppp.data.entity.CategoryEntity;

import java.util.List;

import rx.Observable;

/**
 * Concrete data source providers must implement this. Eg. If data is comming from a
 * database then the database source must implement {@link CategoryDataSource}
 *
 * @author Henry Addo
 */
public interface CategoryDataSource {

    /**
     * Get a {@link Observable} which will emit a List of {@link CategoryEntity}.
     */
    Observable<List<CategoryEntity>> getCategoryEntityList();

    Observable<CategoryEntity> getCategoryEntity(String key);

    /**
     * Adds a {@link CategoryEntity} to storage and then returns an {@link Observable} for all
     * subscribers to react to it.
     *
     * @param categoryEntity The category to be added
     */
    Observable<Void> putCategory(CategoryEntity categoryEntity);

    /**
     * Deletes a {@link CategoryEntity} from storage and then returns an {@link Observable} for all
     * subscribers to react to it.
     *
     * @param key The category key
     */
    Observable<Boolean> deleteCategory(String key);
}
