/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.data.repository.inventory.datasource;

import com.addhen.ppp.data.database.FirebaseRealTimeDatabase;
import com.addhen.ppp.data.entity.InventoryEntity;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Data sources from SQLite database.
 *
 * @author Henry Addo
 */
public class InventoryFirebaseDataSource implements InventoryDataSource {

    private final FirebaseRealTimeDatabase mFirebaseRealTimeDatabase;

    @Inject
    public InventoryFirebaseDataSource(@NonNull FirebaseRealTimeDatabase firebaseRealTimeDatabase) {
        mFirebaseRealTimeDatabase = firebaseRealTimeDatabase;
    }

    @Override
    public Observable<List<InventoryEntity>> getInventoryEntityList(int limit, int page) {
        return mFirebaseRealTimeDatabase.getInventoryEntityList(limit, page);
    }

    @Override
    public Observable<InventoryEntity> getInventoryEntityDetails(final String inventoryId) {
        return mFirebaseRealTimeDatabase.getInventoryEntity(inventoryId);
    }

    @Override
    public Observable<Void> putInventory(InventoryEntity inventory) {
        return mFirebaseRealTimeDatabase.putInventory(inventory);
    }

    @Override
    public Observable<Boolean> deleteInventory(String key) {
        return mFirebaseRealTimeDatabase.deleteInventory(key);
    }
}
