package com.addhen.ppp.data.exception;

/**
 * @author Henry Addo
 */
public class FirebaseInvalidTokenException extends Exception {

    public FirebaseInvalidTokenException() {
        super();
    }

    public FirebaseInvalidTokenException(final String message) {
        super(message);
    }

    public FirebaseInvalidTokenException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FirebaseInvalidTokenException(final Throwable cause) {
        super(cause);
    }
}
