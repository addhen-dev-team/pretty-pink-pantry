package com.addhen.ppp.data.net;


import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.data.net.service.RestfulService;

import java.util.List;

import rx.Observable;

/**
 * Category related API calls
 *
 * @author Henry Addo
 */
public class CategoryApi {

    private final RestfulService mRestfulApi;

    /**
     * Constructs an object of this class
     *
     * @param restfulApi {@link RestfulService}.
     */
    public CategoryApi(RestfulService restfulApi) {
        if (restfulApi == null) {
            throw new IllegalArgumentException("The constructor parameters cannot be null!!!");
        }
        mRestfulApi = restfulApi;
    }

    /**
     * Retrieves reviews attached to a particular movie
     * @return an observable that emits a list of {@link CategoryEntity}
     */
    public Observable<List<CategoryEntity>> getCategoryList() {
        return mRestfulApi.getCategories();
    }
}
