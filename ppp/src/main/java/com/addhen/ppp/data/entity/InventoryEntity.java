/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.data.entity;

import com.addhen.android.raiburari.data.entity.DataEntity;

import java.util.Date;

/**
 * @author Henry Addo
 */
public class InventoryEntity extends DataEntity {

    public String name;

    public float price;

    public String note;

    public CategoryEntity category;

    public Quantity quantity;

    public Date expiryDate;

    public String key;

    public InventoryEntity() {
        // Default constructor required for Firebase calls to DataSnapshot.getValue()
    }

    public InventoryEntity(String key, String name, float price, Quantity quantity,
            String note, CategoryEntity category, Date expiryDate) {
        this.key = key;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.note = note;
        this.category = category;
        this.expiryDate = (Date) expiryDate.clone();
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public String getNote() {
        return note;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    @Override
    public String toString() {
        return "InventoryEntity{"
                + "key='" + key + '\''
                + ", name='" + name + '\''
                + ", price=" + price
                + ", note='" + note + '\''
                + ", category=" + category
                + ", quantity=" + quantity
                + ", expiryDate=" + expiryDate
                + '}';
    }

    public static class Quantity {

        public int initial;

        public int remaining;

        public int reorder;

        public String unit;

        public Quantity() {
            // Default constructor required for Firebase calls to DataSnapshot.getValue()
        }

        public Quantity(int initial, int remaining, int reorder, String unit) {
            this.initial = initial;
            this.remaining = remaining;
            this.reorder = reorder;
            this.unit = unit;
        }

        @Override
        public String toString() {
            return "Quantity{"
                    + "initial=" + initial
                    + ", remaining=" + remaining
                    + ", reorder=" + reorder
                    + ", unit='" + unit + '\''
                    + '}';
        }
    }
}
