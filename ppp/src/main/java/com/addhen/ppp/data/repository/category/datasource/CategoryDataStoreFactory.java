package com.addhen.ppp.data.repository.category.datasource;

import com.addhen.ppp.data.database.FirebaseRealTimeDatabase;

import javax.inject.Inject;

/**
 * @author Henry Addo
 */
public class CategoryDataStoreFactory {

    private final FirebaseRealTimeDatabase mFirebaseRealTimeDatabase;

    @Inject
    public CategoryDataStoreFactory(FirebaseRealTimeDatabase firebaseRealTimeDatabase) {
        if (firebaseRealTimeDatabase == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null");
        }
        mFirebaseRealTimeDatabase = firebaseRealTimeDatabase;
    }

    public CategoryFirebaseDataSource createFirebaseDataStore() {
        return new CategoryFirebaseDataSource(mFirebaseRealTimeDatabase);
    }
}
