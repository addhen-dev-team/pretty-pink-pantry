package com.addhen.ppp.data.exception;

/**
 * @author Henry Addo
 */
public class FirebaseGeneralException extends Exception {

    public FirebaseGeneralException() {
        super();
    }

    public FirebaseGeneralException(final String message) {
        super(message);
    }

    public FirebaseGeneralException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FirebaseGeneralException(final Throwable cause) {
        super(cause);
    }
}
