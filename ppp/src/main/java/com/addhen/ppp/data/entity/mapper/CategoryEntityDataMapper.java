package com.addhen.ppp.data.entity.mapper;

import com.addhen.ppp.data.entity.CategoryEntity;
import com.addhen.ppp.domain.entity.Category;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Henry Addo
 */
public class CategoryEntityDataMapper {

    @Inject
    public CategoryEntityDataMapper() {
        // Do nothing. Needed for dagger
    }

    /**
     * Transform a {@link CategoryEntity} into an {@link Category}.
     *
     * @param categoryEntity Object to be transformed.
     * @return {@link Category} if valid {@link CategoryEntity} otherwise null.
     */
    @Nullable
    public Category map(@NonNull CategoryEntity categoryEntity) {
        Category category = null;
        if (categoryEntity != null) {
            category = new Category(categoryEntity.key, categoryEntity.name);
        }
        return category;
    }

    /**
     * Transform a {@link Category} into an {@link CategoryEntity}.
     *
     * @param category Object to be transformed.
     * @return {@link CategoryEntity} if valid {@link Category} otherwise null.
     */
    @Nullable
    public CategoryEntity unmap(@NonNull Category category) {
        CategoryEntity categoryEntity = null;
        if (category != null) {
            categoryEntity = new CategoryEntity(category.key, category.name);
        }
        return categoryEntity;
    }

    /**
     * Transform a List of {@link CategoryEntity} into a Collection of {@link Category}.
     *
     * @param categoryEntityCollection Object Collection to be transformed.
     * @return {@link Category} if valid {@link CategoryEntity} otherwise null.
     */
    public List<Category> map(@NonNull Collection<CategoryEntity> categoryEntityCollection) {
        List<Category> categoryList = new ArrayList<>();
        Category category;
        for (CategoryEntity categoryEntity : categoryEntityCollection) {
            category = map(categoryEntity);
            if (category != null) {
                categoryList.add(category);
            }
        }
        return categoryList;
    }
}
