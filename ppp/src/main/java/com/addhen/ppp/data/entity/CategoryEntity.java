package com.addhen.ppp.data.entity;

import com.addhen.android.raiburari.data.entity.DataEntity;

/**
 * @author Henry Addo
 */
public class CategoryEntity extends DataEntity {

    public String name;

    public String key;

    public CategoryEntity() {
        // Default constructor required for Firebase calls to DataSnapshot.getValue( )
    }

    public CategoryEntity(String key, String name) {
        this.name = name;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "CategoryEntity{"
                + "name='" + name + '\''
                + ", key='" + key + '\''
                + '}';
    }
}
