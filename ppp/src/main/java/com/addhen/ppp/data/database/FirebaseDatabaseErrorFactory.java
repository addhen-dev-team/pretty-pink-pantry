package com.addhen.ppp.data.database;

import com.google.firebase.database.DatabaseError;

import com.addhen.ppp.data.exception.FirebaseExpiredTokenException;
import com.addhen.ppp.data.exception.FirebaseGeneralException;
import com.addhen.ppp.data.exception.FirebaseInvalidTokenException;
import com.addhen.ppp.data.exception.FirebaseNetworkErrorException;
import com.addhen.ppp.data.exception.FirebaseOperationFailedException;
import com.addhen.ppp.data.exception.FirebasePermissionDeniedException;

import rx.Subscriber;

/**
 * @author Henry Addo
 */
public final class FirebaseDatabaseErrorFactory {

    private FirebaseDatabaseErrorFactory() {
        //empty constructor prevent initialisation
    }

    /**
     * This method add to subscriber the proper error according to the
     *
     * @param subscriber {@link rx.Subscriber}
     * @param error      {@link DatabaseError}
     * @param <T>        generic subscriber
     */
    static <T> void create(Subscriber<T> subscriber, DatabaseError error) {
        switch (error.getCode()) {
            case DatabaseError.INVALID_TOKEN:
                subscriber.onError(new FirebaseInvalidTokenException(error.getMessage()));
                break;
            case DatabaseError.EXPIRED_TOKEN:
                subscriber.onError(new FirebaseExpiredTokenException(error.getMessage()));
                break;
            case DatabaseError.NETWORK_ERROR:
                subscriber.onError(new FirebaseNetworkErrorException(error.getMessage()));
                break;
            case DatabaseError.PERMISSION_DENIED:
                subscriber.onError(new FirebasePermissionDeniedException(error.getMessage()));
                break;
            case DatabaseError.OPERATION_FAILED:
                subscriber.onError(new FirebaseOperationFailedException(error.getMessage()));
                break;
            default:
                subscriber.onError(new FirebaseGeneralException(error.getMessage()));
                break;
        }
    }
}
