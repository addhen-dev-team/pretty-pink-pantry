package com.addhen.ppp.data.exception;

/**
 * @author Henry Addo
 */
public class FirebaseOperationFailedException extends Exception {

    public FirebaseOperationFailedException() {
        super();
    }

    public FirebaseOperationFailedException(final String message) {
        super(message);
    }

    public FirebaseOperationFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FirebaseOperationFailedException(final Throwable cause) {
        super(cause);
    }
}
