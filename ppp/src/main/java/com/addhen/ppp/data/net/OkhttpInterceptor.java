package com.addhen.ppp.data.net;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;


/**
 * A custom Interceptor for the OkHttp for reacting to network issues
 *
 * @author Henry Addo
 */
public class OkhttpInterceptor implements Interceptor {

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        //TODO Handle network errors
        return response;
    }
}
