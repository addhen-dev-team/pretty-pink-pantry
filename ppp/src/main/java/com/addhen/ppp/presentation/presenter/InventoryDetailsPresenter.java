package com.addhen.ppp.presentation.presenter;

import com.addhen.android.raiburari.domain.exception.DefaultErrorHandler;
import com.addhen.android.raiburari.domain.exception.ErrorHandler;
import com.addhen.android.raiburari.domain.usecase.DefaultSubscriber;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.android.raiburari.presentation.presenter.Presenter;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.domain.usecase.inventory.GetInventoryDetailsUsecase;
import com.addhen.ppp.presentation.exception.ErrorMessageFactory;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.model.mapper.InventoryModelMapper;
import com.addhen.ppp.presentation.view.InventoryDetailsView;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Henry Addo
 */
@ActivityScope
public class InventoryDetailsPresenter implements Presenter {

    private final GetInventoryDetailsUsecase mGetInventoryDetailsUsecase;

    private final InventoryModelMapper mInventoryModelMapper;

    private InventoryDetailsView mInventoryDetailsView;

    @Inject
    public InventoryDetailsPresenter(
            @Named("inventoryDetails") GetInventoryDetailsUsecase getInventoryDetailsUsecase,
            InventoryModelMapper inventoryModelMapper) {
        mGetInventoryDetailsUsecase = getInventoryDetailsUsecase;
        mInventoryModelMapper = inventoryModelMapper;
    }

    @Override
    public void resume() {
        //No-Op
    }

    @Override
    public void pause() {
        mGetInventoryDetailsUsecase.unsubscribe();
    }

    @Override
    public void destroy() {
        mGetInventoryDetailsUsecase.unsubscribe();
    }

    public void setView(@NonNull InventoryDetailsView inventoryDetailsView) {
        mInventoryDetailsView = inventoryDetailsView;
    }

    public void initInventory() {
        loadInventoryDetails();
    }

    private void loadInventoryDetails() {
        mInventoryDetailsView.hideLoading();
        mInventoryDetailsView.showLoading();
        mGetInventoryDetailsUsecase.execute(new DefaultSubscriber<Inventory>() {
            @Override
            public void onCompleted() {
                mInventoryDetailsView.hideLoading();
            }

            @Override
            public void onError(Throwable e) {
                mInventoryDetailsView.hideLoading();
                if (e instanceof Exception) {
                    showErrorMessage(new DefaultErrorHandler((Exception) e));
                }
                mInventoryDetailsView.showLoading();
            }

            @Override
            public void onNext(Inventory inventory) {
                showInventoryDetailsInView(inventory);
            }
        });
    }

    private void showErrorMessage(ErrorHandler errorHandler) {
        String errorMessage = ErrorMessageFactory.create(mInventoryDetailsView.getAppContext(),
                errorHandler.getException());
        mInventoryDetailsView.showError(errorMessage);
    }

    private void showInventoryDetailsInView(Inventory inventory) {
        final InventoryModel inventoryModel = mInventoryModelMapper.map(inventory);
        mInventoryDetailsView.showInventoryDetails(inventoryModel);
    }
}
