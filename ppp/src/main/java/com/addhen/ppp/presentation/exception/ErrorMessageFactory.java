/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.presentation.exception;


import com.addhen.ppp.BuildConfig;
import com.addhen.ppp.R;
import com.addhen.ppp.data.exception.CategoryNotFoundException;
import com.addhen.ppp.data.exception.InventoryNotFoundException;
import com.addhen.ppp.data.exception.NetworkConnectionException;

import android.content.Context;

import timber.log.Timber;

/**
 * @author Henry Addo
 */
public final class ErrorMessageFactory {

    private ErrorMessageFactory() {
        // No instances
    }

    /**
     * Creates a String representing an error message.
     *
     * @param context   Context needed to retrieve string resources.
     * @param exception An exception used as a condition to retrieve the correct error message.
     * @return {@link String} an error message.
     */
    public static String create(Context context, Exception exception) {
        String message = context.getString(R.string.exception_message_generic);
        if (exception instanceof InventoryNotFoundException) {
            message = context.getString(R.string.exception_message_inventory_not_found);
        } else if (exception instanceof CategoryNotFoundException) {
            message = context.getString(R.string.exception_message_category_not_found);
        } else if (exception instanceof NetworkConnectionException) {
            message = context.getString(R.string.exception_message_error_network);
        }
        if (BuildConfig.DEBUG) {
            Timber.e(exception.getMessage());
        }
        return message;
    }
}
