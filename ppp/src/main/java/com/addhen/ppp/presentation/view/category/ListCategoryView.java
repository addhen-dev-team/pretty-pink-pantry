package com.addhen.ppp.presentation.view.category;

import com.addhen.android.raiburari.presentation.ui.view.LoadDataView;
import com.addhen.ppp.presentation.model.CategoryModel;

import java.util.List;

/**
 * @author Henry Addo
 */
public interface ListCategoryView extends LoadDataView {

    void categoryList(List<CategoryModel> categoryModelList);
}
