package com.addhen.ppp.presentation.presenter.category;

import com.addhen.android.raiburari.domain.exception.DefaultErrorHandler;
import com.addhen.android.raiburari.domain.exception.ErrorHandler;
import com.addhen.android.raiburari.domain.usecase.DefaultSubscriber;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.android.raiburari.presentation.presenter.Presenter;
import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.domain.usecase.category.AddCategoryUsecase;
import com.addhen.ppp.presentation.exception.ErrorMessageFactory;
import com.addhen.ppp.presentation.view.category.AddCategoryView;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Henry Addo
 */
@ActivityScope
public class CategoryAddPresenter implements Presenter {

    private final AddCategoryUsecase mAddCategoryUsecase;

    private AddCategoryView mAddCategoryView;

    @Inject
    public CategoryAddPresenter(@Named("categoryAdd") AddCategoryUsecase addCategoryUsecase) {
        mAddCategoryUsecase = addCategoryUsecase;
    }

    @Override
    public void resume() {
        // No-op
    }

    @Override
    public void pause() {
        // No-op
    }

    @Override
    public void destroy() {
        mAddCategoryUsecase.unsubscribe();
    }

    public void setAddCategoryView(@NonNull AddCategoryView addCategoryView) {
        mAddCategoryView = addCategoryView;
    }

    public void addCategory(String name) {
        final Category category = new Category(null, name);
        mAddCategoryUsecase.setCategory(category);
        mAddCategoryUsecase.execute(new DefaultSubscriber<Long>() {
            @Override
            public void onError(Throwable e) {
                if (e instanceof Exception) {
                    showErrorMessage(new DefaultErrorHandler((Exception) e));
                }
            }

            @Override
            public void onNext(Long row) {
                mAddCategoryView.onCategoryAdded(row);
            }
        });
    }

    private void showErrorMessage(ErrorHandler errorHandler) {
        String errorMessage = ErrorMessageFactory.create(mAddCategoryView.getAppContext(),
                errorHandler.getException());
        mAddCategoryView.showError(errorMessage);
    }
}
