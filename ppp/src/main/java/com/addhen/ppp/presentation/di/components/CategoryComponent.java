package com.addhen.ppp.presentation.di.components;

import com.addhen.android.raiburari.presentation.di.module.ActivityModule;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.ppp.presentation.di.component.AppComponent;
import com.addhen.ppp.presentation.di.modules.CategoryModule;
import com.addhen.ppp.presentation.presenter.category.CategoryListPresenter;

import dagger.Component;

/**
 * @author Henry Addo
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = {ActivityModule.class,
        CategoryModule.class})
public interface CategoryComponent extends AppActivityComponent {

    CategoryListPresenter categoryListPresenter();
}
