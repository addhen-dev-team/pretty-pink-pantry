package com.addhen.ppp.presentation.di.components.inventory;

import com.addhen.android.raiburari.presentation.di.module.ActivityModule;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.ppp.presentation.di.component.AppComponent;
import com.addhen.ppp.presentation.di.components.AppActivityComponent;
import com.addhen.ppp.presentation.di.modules.CategoryModule;
import com.addhen.ppp.presentation.di.modules.InventoryModule;
import com.addhen.ppp.presentation.presenter.InventoryAddPresenter;
import com.addhen.ppp.presentation.presenter.category.CategoryAddPresenter;
import com.addhen.ppp.presentation.presenter.category.CategoryListPresenter;
import com.addhen.ppp.presentation.view.ui.activities.AddInventoryActivity;
import com.addhen.ppp.presentation.view.ui.fragments.AddInventoryFragment;

import dagger.Component;

/**
 * @author Henry AAddo
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = {ActivityModule.class,
        CategoryModule.class, InventoryModule.class})
public interface AddInventoryComponent extends AppActivityComponent {

    void inject(AddInventoryActivity addInventoryActivity);

    void inject(AddInventoryFragment addInventoryFragment);

    InventoryAddPresenter inventoryAddPresenter();

    CategoryListPresenter categoryListPresenter();

    CategoryAddPresenter categoryAddPresenter();
}
