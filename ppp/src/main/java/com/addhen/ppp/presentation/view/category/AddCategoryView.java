package com.addhen.ppp.presentation.view.category;

import com.addhen.android.raiburari.presentation.ui.view.UiView;

/**
 * Created by eyedol on 5/30/16.
 */

public interface AddCategoryView extends UiView {

    void onCategoryAdded(Long categoryId);
}
