package com.addhen.ppp.presentation.di.modules;

import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.ppp.domain.usecase.category.AddCategoryUsecase;
import com.addhen.ppp.domain.usecase.category.DeleteCategoryUsecase;
import com.addhen.ppp.domain.usecase.category.ListCategoryUsecase;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Category related Dagger DI modules
 *
 * @author Henry Addo
 */
@Module
public class CategoryModule {

    @Provides
    @ActivityScope
    @Named("categoryList")
    Usecase provideGetCategoryListUseCase(
            ListCategoryUsecase getCategoryListUseCase) {
        return getCategoryListUseCase;
    }

    @Provides
    @ActivityScope
    @Named("categoryDelete")
    Usecase provideDeleteCategoryUseCase(
            DeleteCategoryUsecase deleteCategoryUsecase) {
        return deleteCategoryUsecase;
    }

    @Provides
    @ActivityScope
    @Named("categoryAdd")
    AddCategoryUsecase provideAddCategoryUseCase(AddCategoryUsecase addCategoryUsecase) {
        return addCategoryUsecase;
    }
}
