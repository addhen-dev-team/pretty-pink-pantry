package com.addhen.ppp.presentation.view.ui.navigation;

import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.view.ui.activities.AddInventoryActivity;

import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

/**
 * Launches Activities
 *
 * @author Henry Addo
 */
public class Launcher {

    @Inject
    public Launcher() {
        // Do nothing
    }

    /**
     * Launches AddInventoryActivity
     */
    public void launchAddInventory(Context context, InventoryModel inventoryModel) {
        final Intent intent = AddInventoryActivity.getLaunchIntent(context, inventoryModel);
        context.startActivity(intent);
    }
}
