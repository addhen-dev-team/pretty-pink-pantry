package com.addhen.ppp.presentation.view.ui.activities;

import com.addhen.android.raiburari.presentation.di.HasComponent;
import com.addhen.android.raiburari.presentation.ui.activity.BaseActivity;
import com.addhen.ppp.R;
import com.addhen.ppp.presentation.PppApplication;
import com.addhen.ppp.presentation.di.component.AppComponent;
import com.addhen.ppp.presentation.di.components.inventory.DaggerListInventoryComponent;
import com.addhen.ppp.presentation.di.components.inventory.ListInventoryComponent;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.view.ui.fragments.ListInventoryFragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.OnClick;


public class MainActivity extends BaseActivity implements HasComponent<ListInventoryComponent> {

    ListInventoryComponent mListInventoryComponent;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public MainActivity() {
        super(R.layout.activity_main, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mListInventoryComponent == null) {
            mListInventoryComponent = DaggerListInventoryComponent.builder()
                    .appComponent(getAppComponent())
                    .build();
        }
        setSupportActionBar(mToolbar);
        initFragment(savedInstanceState);
    }

    private void initFragment(Bundle savedInstanceState) {
        setSupportActionBar(mToolbar);
        if (savedInstanceState == null) {
            addFragment(R.id.fragment_container, ListInventoryFragment.newInstance());
        }
    }

    @OnClick(R.id.add_inventory_fab)
    public void onFabClick() {
        InventoryModel.QuantityModel quantityModel = new InventoryModel.QuantityModel(
                1,
                1,
                1,
                getString(R.string.none)
        );
        InventoryModel inventoryModel = new InventoryModel(
                null,
                null,
                0.0f,
                quantityModel,
                null,
                null,
                null
        );

        mListInventoryComponent.launcher().launchAddInventory(this, inventoryModel);
    }

    public ListInventoryComponent getComponent() {
        return mListInventoryComponent;
    }

    /**
     * Gets the Main Application component for dependency injection.
     *
     * @return {@link com.addhen.android.raiburari.presentation.di.component.ApplicationComponent}
     */
    public AppComponent getAppComponent() {
        return ((PppApplication) getApplication()).getAppComponent();
    }
}
