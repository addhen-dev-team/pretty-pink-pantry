package com.addhen.ppp.presentation.di.modules;

import com.addhen.android.raiburari.domain.executor.PostExecutionThread;
import com.addhen.android.raiburari.domain.executor.ThreadExecutor;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.ppp.domain.repository.InventoryRepository;
import com.addhen.ppp.domain.usecase.inventory.AddInventoryUsecase;
import com.addhen.ppp.domain.usecase.inventory.DeleteInventoryUsecase;
import com.addhen.ppp.domain.usecase.inventory.FetchInventoryListUsecase;
import com.addhen.ppp.domain.usecase.inventory.GetInventoryDetailsUsecase;
import com.addhen.ppp.domain.usecase.inventory.GetInventoryListUsecase;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module for providing classes to AddInventory use
 *
 * @author Henry Addo
 */
@Module
public class InventoryModule {

    private String mKey;

    public InventoryModule() {
        // No-op
    }

    public InventoryModule(String key) {
        mKey = key;
    }


    @Provides
    @ActivityScope
    @Named("inventoryAdd")
    AddInventoryUsecase provideAddInventoryUseCase(AddInventoryUsecase addInventoryUsecase) {
        return addInventoryUsecase;
    }

    @Provides
    @ActivityScope
    @Named("inventoryList")
    GetInventoryListUsecase provideGetInventoryListUseCase(
            GetInventoryListUsecase getInventoryListUseCase) {
        return getInventoryListUseCase;
    }

    @Provides
    @ActivityScope
    @Named("fetchInventoryList")
    FetchInventoryListUsecase provideFetchInventoryListUseCase(
            FetchInventoryListUsecase getInventoryListUseCase) {
        return getInventoryListUseCase;
    }

    @Provides
    @ActivityScope
    @Named("inventoryDelete")
    Usecase provideDeleteInventoryUseCase(InventoryRepository userRepository,
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {

        return new DeleteInventoryUsecase(mKey, userRepository, threadExecutor,
                postExecutionThread);
    }

    @Provides
    @ActivityScope
    @Named("inventoryDetails")
    GetInventoryDetailsUsecase provideDetailsInventoryUseCase(
            InventoryRepository userRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetInventoryDetailsUsecase(mKey, userRepository, threadExecutor,
                postExecutionThread);
    }

}
