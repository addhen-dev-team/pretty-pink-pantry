package com.addhen.ppp.presentation.view.ui.fragments;

import com.addhen.android.raiburari.presentation.ui.fragment.BaseRecyclerViewFragment;
import com.addhen.android.raiburari.presentation.ui.listener.RecyclerViewItemTouchListenerAdapter;
import com.addhen.android.raiburari.presentation.ui.widget.BloatedRecyclerView;
import com.addhen.ppp.R;
import com.addhen.ppp.presentation.di.components.inventory.ListInventoryComponent;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.presenter.FetchInventoryListPresenter;
import com.addhen.ppp.presentation.presenter.InventoryListPresenter;
import com.addhen.ppp.presentation.view.FetchInventoryListView;
import com.addhen.ppp.presentation.view.InventoryListView;
import com.addhen.ppp.presentation.view.ui.adapters.InventoryAdapter;
import com.addhen.ppp.presentation.view.ui.navigation.Launcher;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

// TODO: Refactor class to remove these suppressions
@SuppressWarnings({"PMD.TooManyMethods", "PMD.GodClass"})
public class ListInventoryFragment
        extends BaseRecyclerViewFragment<InventoryModel, InventoryAdapter>
        implements FetchInventoryListView,
        RecyclerViewItemTouchListenerAdapter.RecyclerViewOnItemClickListener {

    private static final String BUNDLE_STATE_INVENTORY_POSITION
            = "com.addhen.ppp.BUNDLE_STATE_INVENTORY_POSITION";

    @BindView(R.id.loading_container)
    ViewGroup mProgressbarViewGroup;

    @BindView(R.id.retry_container)
    ViewGroup mRetryViewGroup;

    @BindView(android.R.id.empty)
    ViewGroup mEmptyView;

    @Inject
    InventoryListPresenter mInventoryListPresenter;

    @Inject
    FetchInventoryListPresenter mFetchInventoryListPresenter;

    @Inject
    Launcher mLauncher;

    private boolean mIsPaginating;

    private boolean mIsFromInternet;

    private int mPosition;

    /**
     * BaseFragment
     */
    public ListInventoryFragment() {
        super(InventoryAdapter.class, R.layout.fragment_list_inventory, R.menu.main);
        setRetainInstance(true);
    }

    public static ListInventoryFragment newInstance() {
        return new ListInventoryFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent(ListInventoryComponent.class).inject(this);
        if (savedInstanceState != null) {
            mPosition = savedInstanceState.getInt(BUNDLE_STATE_INVENTORY_POSITION, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initialize();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFetchInventoryListPresenter.setView(this);
        mInventoryListPresenter.setView(new ListInventoryView(this));
        if (savedInstanceState != null) {
            mPosition = savedInstanceState.getInt(BUNDLE_STATE_INVENTORY_POSITION);
        }
        // Load from the database
        mInventoryListPresenter.loadInventoryList(1, mIsPaginating);
        mBloatedRecyclerView.recyclerView.smoothScrollToPosition(mPosition);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.mInventoryListPresenter.resume();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(BUNDLE_STATE_INVENTORY_POSITION, mPosition);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.mInventoryListPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mInventoryListPresenter.destroy();
    }

    private void initialize() {
        mBloatedRecyclerView.addItemDividerDecoration(getActivity());
        mBloatedRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mBloatedRecyclerView.enableDefaultSwipeRefresh(true);
        //mBloatedRecyclerView.enableInfiniteScroll();
        mBloatedRecyclerView.setOnLoadMoreListener(new BloatedRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int itemsCount, int maxLastVisiblePosition) {
                mIsPaginating = true;
                mIsFromInternet = true;
                mFetchInventoryListPresenter.loadInventoryList(1, mIsPaginating);
                mPosition = maxLastVisiblePosition;
                mBloatedRecyclerView.recyclerView.getLayoutManager()
                        .scrollToPosition(maxLastVisiblePosition);
            }
        });
        final RecyclerViewItemTouchListenerAdapter itemTouchListenerAdapter
                = new RecyclerViewItemTouchListenerAdapter(mBloatedRecyclerView.recyclerView, this);
        mBloatedRecyclerView.recyclerView.addOnItemTouchListener(itemTouchListenerAdapter);
        mBloatedRecyclerView
                .setDefaultOnRefreshListener(() -> {
                    mBloatedRecyclerView.setRefreshing(true);
                    mIsFromInternet = true;
                    mFetchInventoryListPresenter.loadInventoryList(1, mIsPaginating);
                    // Hide progress bar when pull refresh is in action
                    mProgressbarViewGroup.setVisibility(View.GONE);
                    mBloatedRecyclerView.recyclerView.smoothScrollToPosition(0);
                });
    }

    @Override
    public void showFetchedInventoryList(Collection<InventoryModel> inventoryModelCollection) {
        renderInventoryList(inventoryModelCollection);
    }

    @OnClick(R.id.retry_button)
    void onRetryButtonClicked() {
        if (!mIsFromInternet) {
            mInventoryListPresenter.loadInventoryList(1, mIsPaginating);
            return;
        }
        mFetchInventoryListPresenter.loadInventoryList(1, mIsPaginating);
    }

    @Override
    public void showLoading() {
        mEmptyView.setVisibility(View.GONE);
        mProgressbarViewGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        hideLoadingView();
    }

    @Override
    public void showRetry() {
        showRetryView();
    }

    @Override
    public void hideRetry() {
        mRetryViewGroup.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        showSnackbar(getView(), message);
    }

    @Override
    public Context getAppContext() {
        return getActivity().getApplicationContext();
    }

    @Override
    public void onItemClick(RecyclerView recyclerView, View view, int position) {
        if (mRecyclerViewAdapter.getItems().size() > 0) {
            InventoryModel inventoryModel = mRecyclerViewAdapter.getItem(position);
            mLauncher.launchAddInventory(getContext(), inventoryModel);
        }
    }

    @Override
    public void onItemLongClick(RecyclerView recyclerView, View view, int i) {
        // Do nothing
    }

    private void showRetryView() {
        mEmptyView.setVisibility(View.GONE);
        mRetryViewGroup.setVisibility(View.VISIBLE);
        mBloatedRecyclerView.setRefreshing(false);
    }

    private void renderInventoryList(Collection<InventoryModel> inventoryModelCollection) {
        if (!inventoryModelCollection.isEmpty()) {
            if (mIsPaginating) {
                for (InventoryModel inventoryModel : inventoryModelCollection) {
                    mRecyclerViewAdapter
                            .addItem(inventoryModel, mRecyclerViewAdapter.getAdapterItemCount());
                }
            } else {
                mRecyclerViewAdapter.setItems(new ArrayList<>(inventoryModelCollection));
            }
        }

        if (mRecyclerViewAdapter.getAdapterItemCount() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
            mProgressbarViewGroup.setVisibility(View.GONE);
        }
    }

    private void hideLoadingView() {
        mProgressbarViewGroup.setVisibility(View.GONE);
        mBloatedRecyclerView.setRefreshing(false);
        if (!mIsFromInternet && mRecyclerViewAdapter.getItems().size() == 0) {
            mIsFromInternet = true;
            mFetchInventoryListPresenter.loadInventoryList(1, true);
        }
    }

    private static class ListInventoryView implements InventoryListView {

        private final WeakReference<ListInventoryFragment> mListInventoryFragmentWeakReference;

        public ListInventoryView(ListInventoryFragment inventoryFragment) {
            mListInventoryFragmentWeakReference = new WeakReference<>(inventoryFragment);
        }

        @Override
        public void showInventoryList(Collection<InventoryModel> inventoryModelCollection) {
            if (mListInventoryFragmentWeakReference.get() != null) {
                mListInventoryFragmentWeakReference.get()
                        .renderInventoryList(inventoryModelCollection);
            }
        }

        @Override
        public void showLoading() {
            if (mListInventoryFragmentWeakReference.get() != null) {
                mListInventoryFragmentWeakReference.get().mProgressbarViewGroup
                        .setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void hideLoading() {
            if (mListInventoryFragmentWeakReference.get() != null) {
                mListInventoryFragmentWeakReference.get().hideLoadingView();
            }
        }

        @Override
        public void showRetry() {
            if (mListInventoryFragmentWeakReference.get() != null) {
                mListInventoryFragmentWeakReference.get().showRetryView();
            }
        }

        @Override
        public void hideRetry() {
            if (mListInventoryFragmentWeakReference.get() != null) {
                mListInventoryFragmentWeakReference.get().mRetryViewGroup.setVisibility(View.GONE);
            }
        }

        @Override
        public void showError(String message) {
            if (mListInventoryFragmentWeakReference.get() != null) {
                mListInventoryFragmentWeakReference.get()
                        .showSnackbar(mListInventoryFragmentWeakReference.get().getView(), message);
            }
        }

        @Override
        public Context getAppContext() {
            if (mListInventoryFragmentWeakReference.get() != null) {
                mListInventoryFragmentWeakReference.get().getContext();
            }
            return null;
        }
    }
}
