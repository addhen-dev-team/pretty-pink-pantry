package com.addhen.ppp.presentation.model.mapper;

import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.presentation.model.CategoryModel;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

/**
 * Maps {@link Category} unto {@link CategoryModel}
 *
 * @author Henry Addo
 */
public class CategoryModelMapper {

    @Inject
    public CategoryModelMapper() {
        // Do nothing
    }

    /**
     * Map a {@link Category} into an {@link CategoryModel}.
     *
     * @param category Object to be transformed.
     * @return {@link CategoryModel} if valid {@link Category} otherwise null.
     */
    @Nullable
    public CategoryModel map(@NonNull Category category) {
        CategoryModel categoryModel = null;
        if (category != null) {
            categoryModel = new CategoryModel(category.key, category.name);
        }
        return categoryModel;
    }

    /**
     * Map a {@link CategoryModel} into an {@link Category}.
     *
     * @param categoryModel Object to be mapped.
     * @return {@link Category} if valid {@link CategoryModel} otherwise null.
     */
    @Nullable
    public Category map(@NonNull CategoryModel categoryModel) {
        Category category = null;
        if (categoryModel != null) {
            category = new Category(categoryModel.key, categoryModel.name);
        }
        return category;
    }

    /**
     * Map a List of {@link Category} into a Collection of {@link CategoryModel}.
     *
     * @param categoryEntityCollection Object Collection to be transformed.
     * @return {@link CategoryModel} if valid {@link Category} otherwise null.
     */
    public List<CategoryModel> map(Collection<Category> categoryEntityCollection) {
        List<CategoryModel> categoryList = new ArrayList<>();
        CategoryModel category;
        for (Category categoryEntity : categoryEntityCollection) {
            category = map(categoryEntity);
            if (category != null) {
                categoryList.add(category);
            }
        }
        return categoryList;
    }


}
