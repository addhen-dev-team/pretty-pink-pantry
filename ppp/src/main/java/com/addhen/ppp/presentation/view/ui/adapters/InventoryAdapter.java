/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.presentation.view.ui.adapters;

import com.addhen.android.raiburari.presentation.ui.adapter.BaseRecyclerViewAdapter;
import com.addhen.ppp.R;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.util.Util;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.addhen.ppp.presentation.util.Util.bold;
import static com.addhen.ppp.presentation.util.Util.color;

/**
 * Adapter for holding inventory items
 *
 * @author Henry Addo
 */
public class InventoryAdapter extends BaseRecyclerViewAdapter<InventoryModel> {

    private Context mContext;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        mContext = parent.getContext();
        return new Widgets(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_inventory_item, parent, false));
    }

    @Override
    public int getAdapterItemCount() {
        return getItems().size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (position < getItemCount() && customHeaderView != null ? position
                <= getAdapterItemCount()
                : position < getAdapterItemCount() && customHeaderView != null ? position > 0
                        : getAdapterItemCount() > 0) {
            final InventoryModel inventoryModel = getItem(position);
            ((Widgets) viewHolder).status.setBackgroundColor(setStatusColors(
                    inventoryModel.quantity.initial,
                    inventoryModel.quantity.remaining,
                    inventoryModel.quantity.reorder
            ));
            if (!inventoryModel.expiryDate.before(new Date())) {
                ((Widgets) viewHolder).status.setBackgroundColor(
                        ContextCompat.getColor(mContext, android.R.color.black));
            }
            final CharSequence styledItem = bold(
                    color(Color.BLACK, inventoryModel.item));
            final CharSequence styledQuantity = bold(color(
                    Color.BLACK, String.valueOf(inventoryModel.quantity.remaining)
            ));
            SpannableStringBuilder item = new SpannableStringBuilder();
            item.append(styledItem).append(" ").append("(").append(styledQuantity).append(")");

            ((Widgets) viewHolder).item.setText(item, TextView.BufferType.SPANNABLE);
            if (!TextUtils.isEmpty(inventoryModel.note)) {
                final CharSequence styledNote = color(
                        ContextCompat.getColor(mContext, R.color.text_dark),
                        inventoryModel.note
                );
                SpannableStringBuilder note = new SpannableStringBuilder();
                note.append(styledNote);
                ((Widgets) viewHolder).note.setText(note, TextView.BufferType.SPANNABLE);
            }
            ((Widgets) viewHolder).expiringDate.setText(setExpiringDate(inventoryModel.expiryDate));
        }
    }

    private String setExpiringDate(Date date) {
        int month = Util.differenceInMonths(date, new Date(System.currentTimeMillis()));
        return mContext.getResources().getQuantityString(R.plurals.expiring_date, month, month);
    }

    private int setStatusColors(int quantity, int quantityLeft, int reorder) {
        int half = quantity / 2;
        if (quantityLeft > half) {
            return ContextCompat.getColor(mContext, R.color.mid_green);
        } else if (quantityLeft > reorder) {
            return ContextCompat.getColor(mContext, R.color.sunshine_yellow);
        } else {
            return ContextCompat.getColor(mContext, R.color.lipstick);
        }
    }

    public static class Widgets extends RecyclerView.ViewHolder {

        @BindView(R.id.item)
        TextView item;

        @BindView(R.id.status)
        TextView status;

        @BindView(R.id.note)
        TextView note;

        @BindView(R.id.timestamp)
        TextView expiringDate;

        public Widgets(final View convertView) {
            super(convertView);
            ButterKnife.bind(this, convertView);
        }
    }
}
