package com.addhen.ppp.presentation.presenter;

import com.addhen.android.raiburari.domain.exception.DefaultErrorHandler;
import com.addhen.android.raiburari.domain.exception.ErrorHandler;
import com.addhen.android.raiburari.domain.usecase.DefaultSubscriber;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.android.raiburari.presentation.presenter.Presenter;
import com.addhen.ppp.presentation.exception.ErrorMessageFactory;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.view.DeleteInventoryView;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Henry Addo
 */
public class InventoryDeletePresenter implements Presenter {

    private final Usecase mDeleteInventoryUsecase;

    private DeleteInventoryView mDeleteInventoryView;

    @Inject
    public InventoryDeletePresenter(
            @Named("inventoryDelete") Usecase deleteInventoryUsecase) {
        mDeleteInventoryUsecase = deleteInventoryUsecase;
    }

    @Override
    public void resume() {
        // No-op
    }

    @Override
    public void pause() {
        mDeleteInventoryUsecase.unsubscribe();
    }

    @Override
    public void destroy() {
        mDeleteInventoryUsecase.unsubscribe();
    }

    public void setView(DeleteInventoryView deleteInventoryView) {
        mDeleteInventoryView = deleteInventoryView;
    }

    public void deleteInventory(InventoryModel model) {
        showViewLoading();
        mDeleteInventoryUsecase.execute(new InventoryDeleteSubscriber());
    }

    private void showViewLoading() {
        hideViewRetry();
        mDeleteInventoryView.showLoading();
    }

    private void hideViewLoading() {
        mDeleteInventoryView.hideLoading();
    }

    private void showViewRetry() {
        mDeleteInventoryView.showRetry();
    }

    private void hideViewRetry() {
        mDeleteInventoryView.hideRetry();
    }

    private void showErrorMessage(ErrorHandler errorBundle) {
        String errorMessage = ErrorMessageFactory
                .create(mDeleteInventoryView.getAppContext(), errorBundle.getException());
        mDeleteInventoryView.showError(errorMessage);
    }

    private void showInventoryCollectionInView() {
        mDeleteInventoryView.onInventorySuccessfullyDeleted();
    }

    private class InventoryDeleteSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onCompleted() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideViewLoading();
            if (e instanceof Exception) {
                showErrorMessage(new DefaultErrorHandler((Exception) e));
            }
            showViewRetry();
        }

        @Override
        public void onNext(Boolean status) {
            showInventoryCollectionInView();
        }
    }
}
