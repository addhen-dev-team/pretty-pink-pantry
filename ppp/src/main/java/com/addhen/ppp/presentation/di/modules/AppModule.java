package com.addhen.ppp.presentation.di.modules;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import com.addhen.android.raiburari.presentation.di.module.ApplicationModule;
import com.addhen.ppp.data.repository.category.CategoryDataRepository;
import com.addhen.ppp.data.repository.inventory.InventoryDataRepository;
import com.addhen.ppp.domain.repository.CategoryRepository;
import com.addhen.ppp.domain.repository.InventoryRepository;

import android.text.TextUtils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

/**
 * Resuable Dagger modules for activities related to Inventory manipulations
 *
 * @author Henry Addo
 */
@Module(includes = ApplicationModule.class)
public class AppModule {

    @Provides
    @Singleton
    HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(
                message -> Timber.tag("OkHttp").v(message));
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        return loggingInterceptor;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingStrategy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOfT,
                            JsonDeserializationContext context) throws JsonParseException {
                        final SimpleDateFormat parser = new SimpleDateFormat(
                                "dd-MM-yyyy",
                                Locale.getDefault());
                        try {
                            if (TextUtils.isDigitsOnly(json.getAsString())) {
                                return new Date(json.getAsLong());
                            }
                            return new Date(parser.parse(json.getAsString()).getTime());
                        } catch (ParseException e) {
                            throw new JsonParseException(e);
                        }
                    }
                }).create();
    }

    @Provides
    @Singleton
    InventoryRepository provideInventoryRepository(
            InventoryDataRepository inventoryDataRepository) {
        return inventoryDataRepository;
    }

    @Provides
    @Singleton
    CategoryRepository provideCategoryRepository(CategoryDataRepository categoryDataRepository) {
        return categoryDataRepository;
    }

    @Provides
    @Singleton
    DatabaseReference provideDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference();
    }
}
