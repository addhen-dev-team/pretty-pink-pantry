package com.addhen.ppp.presentation.view.ui.adapters;

import com.addhen.ppp.R;
import com.addhen.ppp.presentation.model.CategoryModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Adapter for note spinner
 *
 * @author Henry Addo
 */
public class CategorySpinnerAdapter extends ArrayAdapter<CategoryModel> {

    private final LayoutInflater mLayoutInflater;

    public CategorySpinnerAdapter(Context context) {
        super(context, 0);
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void addCategories(List<CategoryModel> categoryModels) {
        super.addAll(categoryModels);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        return createViewFromResource(position, view, parent, R.layout.category_spinner_item);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(position, convertView, parent,
                R.layout.category_spinner_dropdown_item);
    }

    private View createViewFromResource(int position, View convertView,
            ViewGroup parent, int resource) {
        View view = convertView;
        Widgets widgets;
        if (view == null) {
            view = mLayoutInflater.inflate(resource, parent, false);
            widgets = new Widgets(view);
            view.setTag(widgets);
        } else {
            widgets = (Widgets) view.getTag();
        }
        widgets.title.setText(getItem(position).name);
        return view;
    }


    private static class Widgets {

        TextView title;

        public Widgets(View convertView) {
            title = (TextView) convertView.findViewById(android.R.id.text1);
        }
    }
}
