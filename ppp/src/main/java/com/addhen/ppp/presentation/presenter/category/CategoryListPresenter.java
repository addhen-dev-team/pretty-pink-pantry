package com.addhen.ppp.presentation.presenter.category;

import com.addhen.android.raiburari.domain.exception.DefaultErrorHandler;
import com.addhen.android.raiburari.domain.exception.ErrorHandler;
import com.addhen.android.raiburari.domain.usecase.DefaultSubscriber;
import com.addhen.android.raiburari.domain.usecase.Usecase;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.android.raiburari.presentation.presenter.Presenter;
import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.presentation.exception.ErrorMessageFactory;
import com.addhen.ppp.presentation.model.mapper.CategoryModelMapper;
import com.addhen.ppp.presentation.view.category.ListCategoryView;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Henry Addo
 */
@ActivityScope
public class CategoryListPresenter implements Presenter {

    private final Usecase mListCategoryUsecase;

    private final CategoryModelMapper mCategoryModelMapper;

    private ListCategoryView mListCategoryView;

    @Inject
    public CategoryListPresenter(@Named("categoryList") Usecase listCategoryUsecase,
            CategoryModelMapper categoryModelMapper) {
        mCategoryModelMapper = categoryModelMapper;
        mListCategoryUsecase = listCategoryUsecase;
    }

    public void setView(@NonNull ListCategoryView listCategoryView) {
        mListCategoryView = listCategoryView;
    }

    @Override
    public void resume() {
        // No-op
    }

    @Override
    public void pause() {
        // No-op
    }

    @Override
    public void destroy() {
        mListCategoryUsecase.unsubscribe();
    }

    public void loadCategories() {
        mListCategoryUsecase.execute(new DefaultSubscriber<List<Category>>() {
            @Override
            public void onCompleted() {
                mListCategoryView.hideLoading();
            }

            @Override
            public void onError(Throwable e) {
                mListCategoryView.hideLoading();
                if (e instanceof Exception) {
                    showErrorMessage(new DefaultErrorHandler((Exception) e));
                }
                mListCategoryView.showRetry();
            }

            @Override
            public void onNext(List<Category> category) {
                mListCategoryView.categoryList(mCategoryModelMapper.map(category));
            }
        });
    }

    private void showErrorMessage(ErrorHandler errorHandler) {
        String errorMessage = ErrorMessageFactory.create(mListCategoryView.getAppContext(),
                errorHandler.getException());
        mListCategoryView.showError(errorMessage);
    }
}
