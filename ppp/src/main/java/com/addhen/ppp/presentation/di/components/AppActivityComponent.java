package com.addhen.ppp.presentation.di.components;

import com.addhen.android.raiburari.presentation.di.component.ApplicationComponent;
import com.addhen.android.raiburari.presentation.di.module.ActivityModule;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.ppp.presentation.view.ui.navigation.Launcher;

import dagger.Component;

/**
 * @author Henry Addo
 */
@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface AppActivityComponent {

    Launcher launcher();
}
