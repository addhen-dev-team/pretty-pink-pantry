package com.addhen.ppp.presentation.view.ui.widgets;

import com.addhen.ppp.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;

/**
 * This is a Simple Widget to make it easier to set numeric value.
 *
 * @author Henry Addo
 */
public class NumberSetterEditText extends AppCompatEditText {

    private static final int LOLLIPOP = 21;

    private float mTextWidth;

    private int mIconPadding;

    private int mIconColor;

    private int mUnderLineColor;

    private int mMaxValue;

    private Paint mPaint;

    public NumberSetterEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public NumberSetterEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attributeSet) {
        TypedArray attrs = context.obtainStyledAttributes(attributeSet,
                R.styleable.NumberSetterEditText, 0, 0);
        if (attrs != null) {
            try {
                mMaxValue = attrs.getInteger(R.styleable.NumberSetterEditText_maxValue, 9999);
                mIconColor = attrs.getColor(R.styleable.NumberSetterEditText_iconColor,
                        ContextCompat.getColor(context, R.color.color_primary));
                mUnderLineColor = attrs.getColor(R.styleable.NumberSetterEditText_underlineColor,
                        ContextCompat.getColor(context, R.color.color_accent));
                mIconPadding = (int) attrs
                        .getDimension(R.styleable.NumberSetterEditText_iconPadding, 24);
            } finally {
                attrs.recycle();
            }
        }
        initView();
    }

    private void initView() {
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(mUnderLineColor);
        mPaint.setStrokeWidth(5f);
        mPaint.setAntiAlias(true);
        final Drawable left = getDrawableCompat(R.drawable.ic_add_circle_black_36dp);
        final Drawable right = getDrawableCompat(R.drawable.ic_remove_circle_black_36dp);
        setCompoundDrawablesWithIntrinsicBounds(left, null, right, null);
        setCompoundDrawablePadding(mIconPadding);
        setInputType(InputType.TYPE_CLASS_NUMBER);
        setGravity(Gravity.START);
        setSingleLine(true);
        final Paint textPaint = new Paint();
        textPaint.setTextSize(getTextSize());
        textPaint.setAntiAlias(true);
        mTextWidth = textPaint.measureText(Integer.toString(mMaxValue));
        setFilters(new InputFilter[]{new InputFilter.LengthFilter(mMaxValue)});
        colorIcon(mIconColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Drawable arrow = getCompoundDrawables()[0];
        setMinWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX,
                mTextWidth + arrow.getBounds().width() * 2,
                getContext().getResources().getDisplayMetrics()));
        setMinHeight((int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_PX, arrow.getBounds().height() * 1.5f,
                        getContext().getResources().getDisplayMetrics()));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final boolean superResult = super.onTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                final int value = getText().length() > 0 ? Integer.parseInt(getText().toString())
                        : 0;
                if (event.getRawX() <= getLeft() + getTotalPaddingStart()) {
                    incrementValue(value);
                } else if (event.getRawX() >= getRight() - getTotalPaddingEnd()) {
                    decrementValue(value);
                }
                return false;
            default:
                return superResult;
        }
    }

    private void incrementValue(int val) {
        int value = val;
        if (value >= 0 && String.valueOf(value).length() <= mMaxValue) {
            value++;
        }
        if (String.valueOf(value).length() > mMaxValue) {
            value--;
        }
        if (value < 0) {
            value = 0;
        }
        setText(String.valueOf(value));
        setSelection(String.valueOf(value).length());
    }

    private void decrementValue(int val) {
        int value = val;
        if (value >= 0 && String.valueOf(value).length() <= mMaxValue) {
            value--;
        }
        if (value < 0) {
            value = 0;
        }
        setText(String.valueOf(value));
        setSelection(String.valueOf(value).length());
    }

    @SuppressWarnings("deprecation")
    @TargetApi(LOLLIPOP)
    private Drawable getDrawableCompat(int resource) {
        if (Build.VERSION.SDK_INT >= LOLLIPOP) {
            return getContext().getResources().getDrawable(resource, null);
        }
        return getContext().getResources().getDrawable(resource);
    }

    private void colorIcon(int color) {
        setColorFilter(getCompoundDrawables()[0], color);
        setColorFilter(getCompoundDrawables()[2], color);
    }

    private void setColorFilter(Drawable drawable, int color) {
        if (drawable == null) {
            return;
        }
        Drawable drawableWrap = DrawableCompat.wrap(drawable);

        DrawableCompat.setTintMode(drawableWrap, PorterDuff.Mode.SRC_IN);
        DrawableCompat.setTintList(drawableWrap, setColorStateList(color));
    }

    private ColorStateList setColorStateList(int color) {
        int[][] states = new int[][]{
                new int[]{-android.R.attr.state_enabled}, // disabled
                new int[]{android.R.attr.state_pressed},  // pressed
                new int[]{}, // default
        };

        int[] colors = new int[]{
                lightenColor(color),
                darkenColor(color),
                color
        };
        return new ColorStateList(states, colors);
    }

    private int darkenColor(int color) {
        float factor = 0.9f;
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        return Color.argb(Color.alpha(color),
                Math.max((int) (r * factor), 0),
                Math.max((int) (g * factor), 0),
                Math.max((int) (b * factor), 0));
    }

    private int lightenColor(int color) {
        float factor = 0.3f;
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        return Color.argb(Color.alpha(color),
                (int) ((r * (1 - factor) / 255 + factor) * 255),
                (int) ((g * (1 - factor) / 255 + factor) * 255),
                (int) ((b * (1 - factor) / 255 + factor) * 255));

    }

    @Override
    public void setBackgroundColor(int color) {
        mUnderLineColor = color;
        mPaint.setColor(color);
        invalidate();
        requestLayout();
    }
}
