package com.addhen.ppp.presentation.view.ui.activities;

import com.addhen.android.raiburari.presentation.di.HasComponent;
import com.addhen.android.raiburari.presentation.ui.activity.BaseActivity;
import com.addhen.ppp.R;
import com.addhen.ppp.presentation.PppApplication;
import com.addhen.ppp.presentation.di.component.AppComponent;
import com.addhen.ppp.presentation.di.components.inventory.AddInventoryComponent;
import com.addhen.ppp.presentation.di.components.inventory.DaggerAddInventoryComponent;
import com.addhen.ppp.presentation.di.modules.InventoryModule;
import com.addhen.ppp.presentation.model.CategoryModel;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.presenter.category.CategoryListPresenter;
import com.addhen.ppp.presentation.util.Util;
import com.addhen.ppp.presentation.view.category.ListCategoryView;
import com.addhen.ppp.presentation.view.ui.fragments.AddInventoryFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment for add a new inventory
 *
 * @author Henry Addo
 */
public class AddInventoryActivity extends BaseActivity
        implements HasComponent<AddInventoryComponent> {

    private static final String BUNDLE_STATE_PARAM_CATEGORY_MODEL_LIST
            = "com.addhen.ppp.BUNDLE_STATE_PARAM_CATEGORY_MODEL_LIST";

    private static final String INTENT_EXTRA_PARAM_INVENTORY_MODEL
            = "com.addhen.inbentori.INTENT_PARAM_INVENTORY_MODEL";

    private static final String INSTANCE_STATE_PARAM_INVENTORY_MODEL
            = "com.addhen.inbentori.STATE_PARAM_INVENTORY_MODEL";

    private static final String FRAG_TAG = "add_inventory";

    CategoryListPresenter mCategoryListPresenter;

    AddInventoryComponent mInventoryComponent;

    private List<CategoryModel> mCategoryModelList;

    private AddInventoryFragment mAddInventoryFragment;

    private InventoryModel mInventoryModel;

    public AddInventoryActivity() {
        super(R.layout.activity_add_inventory, 0);
    }

    public static Intent getLaunchIntent(final Context context, InventoryModel inventoryModel) {
        Intent intent = new Intent(context, AddInventoryActivity.class);
        intent.putExtra(INTENT_EXTRA_PARAM_INVENTORY_MODEL, inventoryModel);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeFragment(savedInstanceState);
        injector();
        mAddInventoryFragment = (AddInventoryFragment) getSupportFragmentManager()
                .findFragmentByTag(FRAG_TAG);
        if (mAddInventoryFragment == null) {
            mAddInventoryFragment = AddInventoryFragment
                    .newInstance((ArrayList<CategoryModel>) mCategoryModelList, mInventoryModel);
            addFragment(R.id.add_frag_container, mAddInventoryFragment, FRAG_TAG);
        }
        initCategories();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            savedInstanceState.putParcelable(INSTANCE_STATE_PARAM_INVENTORY_MODEL, mInventoryModel);
            savedInstanceState.putParcelableArrayList(BUNDLE_STATE_PARAM_CATEGORY_MODEL_LIST,
                    (ArrayList<CategoryModel>) mCategoryModelList);
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mCategoryListPresenter.resume();
        if (Util.isCollectionEmpty(mCategoryModelList)) {
            mCategoryListPresenter.loadCategories();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCategoryListPresenter.destroy();
        mAddInventoryFragment.setCategoryModels(mCategoryModelList);
    }

    private void injector() {
        if (mInventoryComponent == null) {
            DaggerAddInventoryComponent.Builder builder = DaggerAddInventoryComponent.builder()
                    .appComponent(getAppComponent());
            if (isInventoryKeySet()) {
                builder.inventoryModule(new InventoryModule(mInventoryModel.key));
            }
            mInventoryComponent = builder.build();
        }
        mCategoryListPresenter = mInventoryComponent.categoryListPresenter();
    }

    public CategoryListPresenter getCategoryListPresenter() {
        return mCategoryListPresenter;
    }

    /**
     * Initializes this activity.
     */
    private void initializeFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mInventoryModel = getIntent().getParcelableExtra(INTENT_EXTRA_PARAM_INVENTORY_MODEL);
        } else {
            mInventoryModel = savedInstanceState
                    .getParcelable(INSTANCE_STATE_PARAM_INVENTORY_MODEL);
        }
        if (isInventoryKeySet()) {
            setActionBarTitle(getString(R.string.update_inventory));
        }
    }

    private boolean isInventoryKeySet() {
        return mInventoryModel != null && !TextUtils.isEmpty(mInventoryModel.key);
    }

    private void initCategories() {
        mCategoryListPresenter.setView(new ListCategoryView() {
            @Override
            public void categoryList(List<CategoryModel> categoryModelList) {
                mCategoryModelList = categoryModelList;
                mAddInventoryFragment.setCategoryModels(categoryModelList);
            }

            @Override
            public void showLoading() {
                // Do nothing
            }

            @Override
            public void hideLoading() {
                // Do nothing
            }

            @Override
            public void showRetry() {
                // Do nothing
            }

            @Override
            public void hideRetry() {
                // Do nothing
            }

            @Override
            public void showError(String message) {
                showSnackbar(getCurrentFocus(), message);
            }

            @Override
            public Context getAppContext() {
                return AddInventoryActivity.this;
            }
        });
    }

    @Override
    public AddInventoryComponent getComponent() {
        return mInventoryComponent;
    }

    public void reloadCategories() {
        mCategoryListPresenter.loadCategories();
    }

    /**
     * Gets the Main Application component for dependency injection.
     *
     * @return {@link com.addhen.android.raiburari.presentation.di.component.ApplicationComponent}
     */
    public AppComponent getAppComponent() {
        return ((PppApplication) getApplication()).getAppComponent();
    }
}
