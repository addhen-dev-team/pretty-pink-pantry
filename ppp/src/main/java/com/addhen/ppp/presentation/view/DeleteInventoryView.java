package com.addhen.ppp.presentation.view;

import com.addhen.android.raiburari.presentation.ui.view.LoadDataView;

/**
 * Add Inventory view
 *
 * @author Henry Addo
 */
public interface DeleteInventoryView extends LoadDataView {

    void onInventorySuccessfullyDeleted();
}
