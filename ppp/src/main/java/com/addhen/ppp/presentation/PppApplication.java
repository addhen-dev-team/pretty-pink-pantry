package com.addhen.ppp.presentation;

import com.addhen.android.raiburari.presentation.BaseApplication;
import com.addhen.ppp.BuildConfig;
import com.addhen.ppp.presentation.di.component.AppComponent;
import com.squareup.leakcanary.LeakCanary;

import net.danlew.android.joda.JodaTimeAndroid;

import timber.log.Timber;

/**
 * Application entry point
 *
 * @author Henry Addo
 */
public class PppApplication extends BaseApplication {

    AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
    }

    private void initializeInjector() {
        JodaTimeAndroid.init(this);
        // Inject leak canary
        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                return;
            }
            LeakCanary.install(this);
            Timber.plant(new Timber.DebugTree());
        }
        mAppComponent = AppComponent.Initializer.init(this);
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
