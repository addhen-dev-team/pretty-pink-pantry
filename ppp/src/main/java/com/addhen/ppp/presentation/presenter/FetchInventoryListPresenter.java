/*
 * Copyright (c) 2015. Henry Addo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.addhen.ppp.presentation.presenter;

import com.addhen.android.raiburari.domain.exception.DefaultErrorHandler;
import com.addhen.android.raiburari.domain.exception.ErrorHandler;
import com.addhen.android.raiburari.domain.usecase.DefaultSubscriber;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.android.raiburari.presentation.presenter.Presenter;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.domain.usecase.inventory.FetchInventoryListUsecase;
import com.addhen.ppp.presentation.exception.ErrorMessageFactory;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.model.mapper.InventoryModelMapper;
import com.addhen.ppp.presentation.view.FetchInventoryListView;

import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
@ActivityScope
public class FetchInventoryListPresenter implements Presenter {

    private static final int LIMIT = 10;

    private final FetchInventoryListUsecase getInventoryListUseCase;

    private final InventoryModelMapper mInventoryModelDataMapper;

    private FetchInventoryListView viewListView;


    @Inject
    public FetchInventoryListPresenter(
            @Named("fetchInventoryList") FetchInventoryListUsecase getInventoryListInventoryCase,
            InventoryModelMapper inventoryModelDataMapper) {
        getInventoryListUseCase = getInventoryListInventoryCase;
        mInventoryModelDataMapper = inventoryModelDataMapper;
    }

    public void setView(@NonNull FetchInventoryListView view) {
        this.viewListView = view;
    }

    @Override
    public void resume() {
        // Do nothing
    }

    @Override
    public void pause() {
        getInventoryListUseCase.unsubscribe();
    }

    @Override
    public void destroy() {
        getInventoryListUseCase.unsubscribe();
    }

    /**
     * Loads all mInventoryEntities
     */
    public void loadInventoryList(int page, boolean isPaginating) {
        if (!isPaginating) {
            hideViewRetry();
            hideViewLoading();
        }
        showViewLoading();
        getInventoryList(page);
    }

    private void showViewLoading() {
        viewListView.showLoading();
    }

    private void hideViewLoading() {
        viewListView.hideLoading();
    }

    private void showViewRetry() {
        viewListView.showRetry();
    }

    private void hideViewRetry() {
        viewListView.hideRetry();
    }

    private void showErrorMessage(ErrorHandler errorBundle) {
        String errorMessage = ErrorMessageFactory.create(viewListView.getAppContext(),
                errorBundle.getException());
        viewListView.showError(errorMessage);
    }

    private void showInventoryCollectionInView(List<Inventory> inventoryCollection) {
        final Collection<InventoryModel> inventoryModelsCollection = mInventoryModelDataMapper
                .map(inventoryCollection);
        viewListView.showFetchedInventoryList(inventoryModelsCollection);
    }

    private void getInventoryList(int page) {
        getInventoryListUseCase.setPage(LIMIT, page);
        getInventoryListUseCase.execute(new InventoryListSubscriber());
    }

    private class InventoryListSubscriber extends DefaultSubscriber<List<Inventory>> {

        @Override
        public void onCompleted() {
            hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideViewLoading();
            if (e instanceof Exception) {
                showErrorMessage(new DefaultErrorHandler((Exception) e));
            }
            showViewRetry();
        }

        @Override
        public void onNext(List<Inventory> inventories) {
            showInventoryCollectionInView(inventories);
        }
    }
}


