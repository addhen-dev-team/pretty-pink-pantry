package com.addhen.ppp.presentation.model;

import com.addhen.android.raiburari.presentation.model.Model;

/**
 * @author Henry Addo
 */
public class PresentationModel extends Model {

    /**
     * The entities ID
     */
    public String key;

    protected PresentationModel(String key) {
        this.key = key;
    }
}
