package com.addhen.ppp.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


/**
 * Inventory Model
 *
 * @author Henry Addo
 */
public class InventoryModel extends PresentationModel implements Parcelable {

    /**
     * Creates a parcelable {@link InventoryModel} type
     */
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<InventoryModel> CREATOR
            = new Parcelable.Creator<InventoryModel>() {
        @Override
        public InventoryModel createFromParcel(Parcel in) {
            return new InventoryModel(in);
        }

        @Override
        public InventoryModel[] newArray(int size) {
            return new InventoryModel[size];
        }
    };

    public String item;

    public QuantityModel quantity;

    public float price;


    public String note;

    public CategoryModel category;

    public Date expiryDate;


    public InventoryModel(String key, String item, float price, QuantityModel quantity,
            String note, CategoryModel category, Date expiryDate) {
        super(key);
        this.item = item;
        this.quantity = quantity;
        this.price = price;
        this.note = note;
        this.category = category;
        if (expiryDate != null) {
            this.expiryDate = (Date) expiryDate.clone();
        }
    }

    protected InventoryModel(Parcel in) {
        super(in.readString());
        this.item = in.readString();
        this.quantity = (QuantityModel) in.readValue(QuantityModel.class.getClassLoader());
        this.price = in.readFloat();
        this.note = in.readString();
        this.category = (CategoryModel) in.readValue(CategoryModel.class.getClassLoader());
        long tmpExpiryDate = in.readLong();
        this.expiryDate = tmpExpiryDate != -1 ? new Date(tmpExpiryDate) : null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(item);
        dest.writeValue(quantity);
        dest.writeFloat(price);
        dest.writeString(note);
        dest.writeValue(category);
        dest.writeLong(expiryDate != null ? expiryDate.getTime() : -1L);
    }

    @Override
    public String toString() {
        return "InventoryModel{"
                + "id='" + _id + '\''
                + "item='" + item + '\''
                + ", quantity=" + quantity
                + ", price=" + price
                + ", note='" + note + '\''
                + ", category=" + category
                + ", expiryDate=" + expiryDate
                + ", key=" + key
                + '}';
    }

    public static class QuantityModel implements Parcelable {

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<QuantityModel> CREATOR
                = new Parcelable.Creator<QuantityModel>() {
            @Override
            public QuantityModel createFromParcel(Parcel in) {
                return new QuantityModel(in);
            }

            @Override
            public QuantityModel[] newArray(int size) {
                return new QuantityModel[size];
            }
        };

        public int initial;

        public int remaining;

        public int reorder;

        public String unit;

        public QuantityModel(int initial, int remaining, int reorder, String unit) {
            this.initial = initial;
            this.remaining = remaining;
            this.reorder = reorder;
            this.unit = unit;
        }

        protected QuantityModel(Parcel in) {
            initial = in.readInt();
            remaining = in.readInt();
            reorder = in.readInt();
            unit = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(initial);
            dest.writeInt(remaining);
            dest.writeInt(reorder);
            dest.writeString(unit);
        }

        @Override
        public String toString() {
            return "QuantityModel{"
                    + "initial=" + initial
                    + ", remaining=" + remaining
                    + ", reorder=" + reorder
                    + ", unit='" + unit + '\''
                    + '}';
        }
    }
}
