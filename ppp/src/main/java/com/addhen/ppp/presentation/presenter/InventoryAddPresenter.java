package com.addhen.ppp.presentation.presenter;

import com.addhen.android.raiburari.domain.exception.DefaultErrorHandler;
import com.addhen.android.raiburari.domain.exception.ErrorHandler;
import com.addhen.android.raiburari.domain.usecase.DefaultSubscriber;
import com.addhen.android.raiburari.presentation.di.qualifier.ActivityScope;
import com.addhen.android.raiburari.presentation.presenter.Presenter;
import com.addhen.ppp.domain.usecase.inventory.AddInventoryUsecase;
import com.addhen.ppp.presentation.exception.ErrorMessageFactory;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.model.mapper.InventoryModelMapper;
import com.addhen.ppp.presentation.view.AddInventoryView;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Presenter for adding a new {@link com.addhen.ppp.presentation.model.InventoryModel} to a
 * storage
 *
 * @author Henry Addo
 */
@ActivityScope
public class InventoryAddPresenter implements Presenter {

    private final AddInventoryUsecase mAddInventoryUsecase;

    private final InventoryModelMapper mInventoryModelMapper;

    private AddInventoryView mAddInventoryView;

    private InventoryModel mInventoryModel;

    @Inject
    public InventoryAddPresenter(@Named("inventoryAdd") AddInventoryUsecase addInventoryUsecase,
            InventoryModelMapper inventoryModelMapper) {
        mInventoryModelMapper = inventoryModelMapper;
        mAddInventoryUsecase = addInventoryUsecase;
    }

    @Override
    public void resume() {
        // No-op
    }

    @Override
    public void pause() {
        mAddInventoryUsecase.unsubscribe();
    }

    @Override
    public void destroy() {
        mAddInventoryUsecase.unsubscribe();
    }

    public void setView(@NonNull AddInventoryView addInventoryView) {
        mAddInventoryView = addInventoryView;
    }

    public void addInventory(InventoryModel inventoryModel) {
        mInventoryModel = inventoryModel;
        addInventory();
    }

    private void addInventory() {
        mAddInventoryView.hideRetry();
        mAddInventoryView.showLoading();
        mAddInventoryUsecase.setInventory(mInventoryModelMapper.map(mInventoryModel));
        mAddInventoryUsecase.execute(new DefaultSubscriber<Void>() {
            @Override
            public void onCompleted() {
                mAddInventoryView.hideLoading();
            }

            @Override
            public void onError(Throwable e) {
                mAddInventoryView.hideLoading();
                if (e instanceof Exception) {
                    showErrorMessage(new DefaultErrorHandler((Exception) e));
                }
                mAddInventoryView.showRetry();
            }

            @Override
            public void onNext(Void aVoid) {
                mAddInventoryView.onInventorySuccessfullyAdded();
            }
        });
    }

    private void showErrorMessage(ErrorHandler errorHandler) {
        String errorMessage = ErrorMessageFactory.create(mAddInventoryView.getAppContext(),
                errorHandler.getException());
        mAddInventoryView.showError(errorMessage);
    }
}
