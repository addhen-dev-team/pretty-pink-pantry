package com.addhen.ppp.presentation.util;

import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.YearMonth;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AlignmentSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.LeadingMarginSpan;
import android.text.style.StyleSpan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

/**
 * @author Henry Addo
 */
public final class Util {

    private Util() {
        // No instantiation
    }

    public static long calculateDateDifference(long startDateInMilli, long endDateInMilli) {
        long periodSeconds = (startDateInMilli - endDateInMilli) / 1000;
        long elapsedDays = periodSeconds / 60 / 60 / 24;
        return elapsedDays / 30;
    }

    public static YearMonth toYearMonth(LocalDate localDate) {
        return new YearMonth(localDate.getYear(), localDate.getMonthOfYear());
    }

    public static int monthSwitches(LocalDate start, LocalDate end) {
        final LocalDate startDate = start.withDayOfMonth(1);
        final LocalDate endDate = end.withDayOfMonth(1);
        return Months.monthsBetween(startDate, endDate).getMonths();
    }

    public static int differenceInMonths(Date d1, Date d2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);
        int diff = 0;
        if (c2.after(c1)) {
            while (c2.after(c1)) {
                c1.add(Calendar.MONTH, 1);
                if (c2.after(c1)) {
                    diff++;
                }
            }
        } else if (c2.before(c1)) {
            while (c2.before(c1)) {
                c1.add(Calendar.MONTH, -1);
                if (c1.before(c2)) {
                    diff--;
                }
            }
        }
        return diff;
    }

    public static boolean isCollectionEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    @Nullable
    public static Date formatDateString(@NonNull String dateString) {
        try {
            Timber.i("Utility: %s", dateString);
            return new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(dateString);
        } catch (ParseException e) {
            Timber.e(e.getMessage());
        }
        Timber.i("Utility: %s", dateString);
        return null;
    }

    @Nullable
    public static String formatEDDMMMYYY(@NonNull String dateString) {
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat parser = new SimpleDateFormat("E, dd MMM, yyyy", Locale.getDefault());
        SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.getDefault());

        try {
            Date date = parser.parse(dateString);
            String formatted = format.format(date);
            Date formattedDate = format.parse(formatted);
            return formatDate(formattedDate, pattern);
        } catch (ParseException e) {
            Timber.e(e.getMessage());
        }
        return null;
    }

    @NonNull
    public static String formDate(@NonNull String dateString) {
        Date date = formatDateString(dateString);
        return formDate(date);
    }

    @Nullable
    public static String formDate(@NonNull Date date) {
        return formatDate(date, "EE, dd MMM, yyyy");
    }

    public static String formatDate(@NonNull Date date, @NonNull String pattern) {
        return new SimpleDateFormat(pattern, Locale.getDefault()).format(date);
    }

    /**
     * Returns a CharSequence that concatenates the specified array of CharSequence
     * objects and then applies a list of zero or more tags to the entire range.
     *
     * @param content an array of character sequences to apply a style to
     * @param tags    the styled span objects to apply to the content
     *                such as android.text.style.StyleSpan
     */
    private static CharSequence apply(CharSequence[] content, Object... tags) {
        SpannableStringBuilder text = new SpannableStringBuilder();
        openTags(text, tags);
        for (CharSequence item : content) {
            text.append(item);
        }
        closeTags(text, tags);
        return text;
    }

    /**
     * Iterates over an array of tags and applies them to the beginning of the specified
     * Spannable object so that future text appended to the text will have the styling
     * applied to it. Do not call this method directly.
     */
    private static void openTags(Spannable text, Object... tags) {
        for (Object tag : tags) {
            text.setSpan(tag, 0, 0, Spannable.SPAN_MARK_MARK);
        }
    }

    /**
     * "Closes" the specified tags on a Spannable by updating the spans to be
     * endpoint-exclusive so that future text appended to the end will not take
     * on the same styling. Do not call this method directly.
     */
    private static void closeTags(Spannable text, Object... tags) {
        int len = text.length();
        for (Object tag : tags) {
            if (len > 0) {
                text.setSpan(tag, 0, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else {
                text.removeSpan(tag);
            }
        }
    }

    /**
     * Returns a CharSequence that applies a foreground color to the
     * concatenation of the specified CharSequence objects.
     */
    public static CharSequence color(int color, CharSequence... content) {
        return apply(content, new ForegroundColorSpan(color));
    }

    /**
     * Returns a CharSequence that applies center alignment to the concatenated
     * CharSequence objects.
     */
    public static CharSequence alignText(Layout.Alignment alignment, CharSequence... content) {
        return apply(content, new AlignmentSpan.Standard(alignment));
    }

    public static CharSequence leadingMargin(int margin, CharSequence... content) {
        return apply(content, new LeadingMarginSpan.Standard(margin));
    }

    /**
     * Returns a CharSequence that applies boldface to the concatenation
     * of the specified CharSequence objects.
     */
    public static CharSequence bold(CharSequence... content) {
        return apply(content, new StyleSpan(Typeface.BOLD));
    }
}
