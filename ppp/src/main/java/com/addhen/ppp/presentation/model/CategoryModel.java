package com.addhen.ppp.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Category Model
 */
public class CategoryModel extends PresentationModel implements Parcelable {

    public static final Creator<CategoryModel> CREATOR = new Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel in) {
            return new CategoryModel(in);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };

    public String name;

    public CategoryModel(String key, String name) {
        super(key);
        this.name = name;
    }

    protected CategoryModel(Parcel in) {
        super(in.readString());
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(name);
    }

    @Override
    public String toString() {
        return "CategoryModel{"
                + "id='" + _id + '\''
                + "name='" + name + '\''
                + "key='" + key + '\''
                + '}';
    }
}
