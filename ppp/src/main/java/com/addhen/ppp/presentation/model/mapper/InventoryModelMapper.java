package com.addhen.ppp.presentation.model.mapper;

import com.addhen.ppp.domain.entity.Category;
import com.addhen.ppp.domain.entity.Inventory;
import com.addhen.ppp.presentation.model.CategoryModel;
import com.addhen.ppp.presentation.model.InventoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

/**
 * Maps {@link Inventory} unto {@link InventoryModel}
 *
 * @author Henry Addo
 */
public class InventoryModelMapper {

    private final CategoryModelMapper mCategoryModelMapper;

    @Inject
    public InventoryModelMapper(CategoryModelMapper categoryModelMapper) {
        mCategoryModelMapper = categoryModelMapper;
    }

    /**
     * Map a {@link Inventory} into an {@link InventoryModel}.
     *
     * @param inventory Object to be transformed.
     * @return {@link InventoryModel} if valid {@link Inventory} otherwise null.
     */
    public InventoryModel map(Inventory inventory) {
        InventoryModel inventoryModel = null;
        if (inventory != null) {
            inventoryModel = new InventoryModel(
                    inventory.key,
                    inventory.item,
                    inventory.price,
                    map(inventory.quantity),
                    inventory.note,
                    map(inventory.category),
                    inventory.expiryDate
            );
        }
        return inventoryModel;
    }

    /**
     * Map a {@link Inventory} into an {@link InventoryModel}.
     *
     * @param inventoryModel Object to be mapped.
     * @return {@link InventoryModel} if valid {@link Inventory} otherwise null.
     */
    public Inventory map(InventoryModel inventoryModel) {
        Inventory inventory = null;
        if (inventoryModel != null) {
            inventory = new Inventory(
                    inventoryModel.key,
                    inventoryModel.item,
                    inventoryModel.price,
                    map(inventoryModel.quantity),
                    inventoryModel.note,
                    mCategoryModelMapper.map(inventoryModel.category),
                    inventoryModel.expiryDate
            );
        }
        return inventory;
    }

    /**
     * Map a List of {@link Inventory} into a Collection of {@link InventoryModel}.
     *
     * @param inventoryEntityCollection Object Collection to be transformed.
     * @return {@link InventoryModel} if valid {@link Inventory} otherwise null.
     */
    public List<InventoryModel> map(Collection<Inventory> inventoryEntityCollection) {
        List<InventoryModel> inventoryList = new ArrayList<>();
        InventoryModel inventory;
        for (Inventory inventoryEntity : inventoryEntityCollection) {
            inventory = map(inventoryEntity);
            if (inventory != null) {
                inventoryList.add(inventory);
            }
        }
        return inventoryList;
    }

    private CategoryModel map(Category category) {
        return mCategoryModelMapper.map(category);
    }

    private InventoryModel.QuantityModel map(Inventory.Quantity quantity) {
        return new InventoryModel.QuantityModel(
                quantity.initial,
                quantity.remaining,
                quantity.reorder,
                quantity.unit
        );
    }

    private Inventory.Quantity map(InventoryModel.QuantityModel quantity) {
        return new Inventory.Quantity(
                quantity.initial,
                quantity.remaining,
                quantity.reorder,
                quantity.unit
        );
    }
}
