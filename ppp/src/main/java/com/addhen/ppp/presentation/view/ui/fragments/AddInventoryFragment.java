package com.addhen.ppp.presentation.view.ui.fragments;

import com.addhen.android.raiburari.presentation.ui.fragment.BaseFragment;
import com.addhen.ppp.R;
import com.addhen.ppp.presentation.di.components.inventory.AddInventoryComponent;
import com.addhen.ppp.presentation.model.CategoryModel;
import com.addhen.ppp.presentation.model.InventoryModel;
import com.addhen.ppp.presentation.presenter.InventoryAddPresenter;
import com.addhen.ppp.presentation.presenter.InventoryDeletePresenter;
import com.addhen.ppp.presentation.presenter.category.CategoryAddPresenter;
import com.addhen.ppp.presentation.util.Util;
import com.addhen.ppp.presentation.view.AddInventoryView;
import com.addhen.ppp.presentation.view.DeleteInventoryView;
import com.addhen.ppp.presentation.view.category.AddCategoryView;
import com.addhen.ppp.presentation.view.ui.activities.AddInventoryActivity;
import com.addhen.ppp.presentation.view.ui.adapters.CategorySpinnerAdapter;
import com.addhen.ppp.presentation.view.ui.widgets.NumberSetterEditText;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemSelected;

/**
 * Fragment for adding a new inventory
 *
 * @author Henry Addo
 */
// TODO: Refactor class to remove these suppressions
@SuppressWarnings({"PMD.TooManyFields", "PMD.TooManyMethods", "PMD.GodClass"})
public class AddInventoryFragment extends BaseFragment
        implements AddInventoryView, AddCategoryView,
        DatePickerDialog.OnDateSetListener {

    private static final String ARGUMENT_KEY_CATEGORY_MODEL_LIST
            = "com.addhen.ppp.ARGUMENT_TRACK_MODEL_LIST";

    private static final String ARGUMENT_KEY_INVENTORY_MODEL
            = "com.addhen.ppp.ARGUMENT_INVENTORY_MODEL";

    @Inject
    CategoryAddPresenter mCategoryAddPresenter;

    @Inject
    InventoryAddPresenter mAddInventoryPresenter;

    @Inject
    InventoryDeletePresenter mInventoryDeletePresenter;

    @BindView(R.id.add_inventory_name)
    AppCompatEditText mNameEditText;

    @BindView(R.id.add_inventory_quantity)
    NumberSetterEditText mQuantityNumberSetterEdit;

    @BindView(R.id.add_inventory_quantity_unit)
    AppCompatSpinner mQuantityUnitSpinner;

    @BindView(R.id.reorder_level_unit_label)
    AppCompatTextView mReorderUnitLabelTextView;

    @BindView(R.id.add_inventory_reorder_level)
    NumberSetterEditText mReorderLevelNumberSetterEdit;

    @BindView(R.id.add_inventory_price)
    AppCompatEditText mPriceEditText;

    @BindView(R.id.add_inventory_category)
    AppCompatSpinner mCategorySpinner;

    @BindView(R.id.add_inventory_expiry_date)
    AppCompatTextView mExpiryDateTextView;

    @BindView(R.id.add_inventory_note)
    AppCompatEditText mNoteEditText;

    @BindView(R.id.add_inventory_delete)
    AppCompatButton mDeleteInventoryButton;

    @BindView(R.id.login_progress_bar)
    ProgressBar mProgressBar;

    private InventoryModel mInventoryModel;

    private CategorySpinnerAdapter mCategorySpinnerAdapter;

    private List<CategoryModel> mCategoryModelList;

    private String mExpiryDate;

    public AddInventoryFragment() {
        super(R.layout.fragment_add_inventory, R.menu.menu_add_inventory);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    public static AddInventoryFragment newInstance(
            @NonNull List<CategoryModel> categoryModels, @Nullable InventoryModel inventoryModel) {
        AddInventoryFragment addInventoryFragment = new AddInventoryFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ARGUMENT_KEY_CATEGORY_MODEL_LIST,
                (ArrayList<CategoryModel>) categoryModels);
        bundle.putParcelable(ARGUMENT_KEY_INVENTORY_MODEL, inventoryModel);
        addInventoryFragment.setArguments(bundle);
        return addInventoryFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent(AddInventoryComponent.class).inject(this);
        mCategoryModelList = getArguments()
                .getParcelableArrayList(ARGUMENT_KEY_CATEGORY_MODEL_LIST);
        mInventoryModel = getArguments().getParcelable(ARGUMENT_KEY_INVENTORY_MODEL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initView();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAddInventoryPresenter.resume();
        mCategoryAddPresenter.resume();
        mInventoryDeletePresenter.resume();
        setCategoryModels(mCategoryModelList);
    }

    @Override
    public void onPause() {
        super.onPause();
        mAddInventoryPresenter.pause();
        mCategoryAddPresenter.pause();
        mInventoryDeletePresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAddInventoryPresenter.destroy();
        mCategoryAddPresenter.destroy();
        mInventoryDeletePresenter.destroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_add) {
            submit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setCategoryModels(@NonNull final List<CategoryModel> categoryModels) {
        if (!Util.isCollectionEmpty(categoryModels)) {
            mCategorySpinnerAdapter.addCategories(categoryModels);
        }
    }

    private void initialize() {
        mAddInventoryPresenter.setView(this);
        mCategoryAddPresenter.setAddCategoryView(this);
        mInventoryDeletePresenter.setView(new InventoryDeleteView(this));
    }

    private void initView() {
        mCategorySpinnerAdapter = new CategorySpinnerAdapter(getContext());
        mCategorySpinner.setAdapter(mCategorySpinnerAdapter);
        // TODO: Clean this mess
        int quantity = 1;
        int reorderLevel = 1;
        String unit = mQuantityUnitSpinner.getSelectedItem().toString();
        int categoryPosition = mCategorySpinner.getSelectedItemPosition();
        int unitPosition = mQuantityUnitSpinner.getSelectedItemPosition();
        String name = null;
        String note = null;
        String price = "00.00";
        String expiryDate = null;
        // Check for edit to an existing inventory. Then initialize widgets with their
        // respective values
        if (mInventoryModel != null && !TextUtils.isEmpty(mInventoryModel.key)) {
            quantity = mInventoryModel.quantity.remaining;
            reorderLevel = mInventoryModel.quantity.reorder;
            unit = mInventoryModel.quantity.unit;
            categoryPosition = mCategorySpinnerAdapter.getPosition(mInventoryModel.category);
            name = mInventoryModel.item;
            note = mInventoryModel.note;
            price = String.valueOf(mInventoryModel.price);
            expiryDate = Util.formDate(mInventoryModel.expiryDate);
            mExpiryDate = Util.formatEDDMMMYYY(Util.formDate(mInventoryModel.expiryDate));
            mDeleteInventoryButton.setVisibility(View.VISIBLE);
        }
        mNameEditText.setText(name);
        mNoteEditText.setText(note);
        mPriceEditText.setText(price);
        mCategorySpinner.setSelection(categoryPosition, true);
        mExpiryDateTextView.setText(expiryDate);
        mQuantityUnitSpinner.setSelection(unitPosition, true);
        mQuantityNumberSetterEdit.setText(String.valueOf(quantity));
        mReorderLevelNumberSetterEdit.setText(String.valueOf(reorderLevel));
        mReorderUnitLabelTextView.setText(unit);
    }

    @Override
    public void showLoading() {
        // Do nothing
    }

    @Override
    public void hideLoading() {
        // Do nothing
    }

    @Override
    public void showRetry() {
        // Do nothing
    }

    @Override
    public void hideRetry() {
        // Do nothing
    }

    @Override
    public void showError(String s) {
        showSnackbar(getView(), s);
    }

    @Override
    public Context getAppContext() {
        return getActivity().getApplicationContext();
    }

    @Override
    public void onInventorySuccessfullyAdded() {
        showToast(R.string.add_inventory_added);
        getActivity().finish();
    }

    private void submit() {
        if (TextUtils.isEmpty(mNameEditText.getText())) {
            mNameEditText.setError(getString(R.string.error_message_inventory_name));
            return;
        }
        mNameEditText.setError(null);
        if (TextUtils.isEmpty(mPriceEditText.getText())) {
            mPriceEditText.setText(getString(R.string.intial_price));
            return;
        }
        mPriceEditText.setError(null);
        if (TextUtils.isEmpty(mExpiryDateTextView.getText())) {
            mExpiryDateTextView.setError(getString(R.string.error_message_inventory_expiry_date));
            return;
        }
        mExpiryDateTextView.setError(null);
        final int quantity = Integer.parseInt(mQuantityNumberSetterEdit.getText().toString());
        final int reorder = Integer.parseInt(mReorderLevelNumberSetterEdit.getText().toString());
        if (reorder >= quantity) {
            mReorderLevelNumberSetterEdit
                    .setError(getString(R.string.error_message_inventory_reorder_level));
            return;
        }
        mReorderLevelNumberSetterEdit.setError(null);
        if (mInventoryModel.key == null) {
            // Only update initial when adding a new inventory
            mInventoryModel.quantity.initial = quantity;
        } else {
            mInventoryModel.quantity.remaining = quantity;
        }
        mInventoryModel.quantity.unit = mQuantityUnitSpinner.getSelectedItem().toString();
        mInventoryModel.item = mNameEditText.getText().toString();
        mInventoryModel.price = Float.parseFloat(mPriceEditText.getText().toString());
        mInventoryModel.note = mNoteEditText.getText().toString();
        mInventoryModel.category = ((CategoryModel) mCategorySpinner.getSelectedItem());
        mInventoryModel.expiryDate = Util.formatDateString(mExpiryDate);
        mAddInventoryPresenter.addInventory(mInventoryModel);
    }

    @OnClick(R.id.add_inventory_expiry_date)
    void onDatePickerClick() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.DAY_OF_YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month,
                day);
        datePickerDialog.show();
    }

    @OnClick(R.id.add_inventory_edit_category)
    void onEditCategoryButtonClicked() {
        showCreateCategoryDialog();
    }

    @OnClick(R.id.add_inventory_delete)
    void setDeleteInventorButtonClicked() {
        mInventoryDeletePresenter.deleteInventory(mInventoryModel);
    }

    @OnItemSelected(R.id.add_inventory_quantity_unit)
    void onQuantityUnitSelected(int position) {
        if (mQuantityUnitSpinner.getAdapter() != null) {
            mReorderUnitLabelTextView
                    .setText(mQuantityUnitSpinner.getAdapter().getItem(position).toString());
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mExpiryDate = String
                .format(Locale.getDefault(), "%02d/%02d/%s", dayOfMonth, monthOfYear + 1, year);
        mExpiryDateTextView
                .setTextColor(ContextCompat.getColor(getActivity(), android.R.color.black));
        mExpiryDateTextView.setText(Util.formDate(mExpiryDate));
    }

    @SuppressLint("InflateParams")
    private void showCreateCategoryDialog() {
        final View createCategoryView = LayoutInflater.from(getContext())
                .inflate(R.layout.add_new_category_inventory_layout, null);
        final AlertDialog.Builder createCategoryDialog = new AlertDialog.Builder(getContext(),
                R.style.CreateCategoryAlertDialog);
        createCategoryDialog.setTitle(R.string.add_category_dialog_title);
        createCategoryDialog.setView(createCategoryView);
        createCategoryDialog.setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel())
                .setPositiveButton(getString(R.string.create),
                        (dialog, which) -> {
                            final AppCompatEditText createCategoryEditText
                                    = (AppCompatEditText) createCategoryView
                                    .findViewById(R.id.add_category_name);
                            final String categoryName = createCategoryEditText.getText().toString();
                            if (!TextUtils.isEmpty(categoryName)) {
                                this.mCategoryAddPresenter.addCategory(categoryName);
                            }
                            dialog.dismiss();
                        }).create().show();
    }

    @Override
    public void onCategoryAdded(Long categoryId) {
        ((AddInventoryActivity) getActivity()).getCategoryListPresenter().loadCategories();
    }

    private static class InventoryDeleteView implements DeleteInventoryView {

        private final WeakReference<AddInventoryFragment> mAddInventoryFragmentWeakReference;

        public InventoryDeleteView(AddInventoryFragment addInventoryFragment) {
            mAddInventoryFragmentWeakReference = new WeakReference<>(addInventoryFragment);
        }

        @Override
        public void onInventorySuccessfullyDeleted() {
            if (mAddInventoryFragmentWeakReference.get() != null) {
                mAddInventoryFragmentWeakReference.get().showToast(R.string.add_inventory_deleted);
                mAddInventoryFragmentWeakReference.get().getActivity().finish();
            }
        }

        @Override
        public void showLoading() {
            if (mAddInventoryFragmentWeakReference.get() != null) {
                mAddInventoryFragmentWeakReference.get().mDeleteInventoryButton
                        .setVisibility(View.GONE);
                mAddInventoryFragmentWeakReference.get().mProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void hideLoading() {
            if (mAddInventoryFragmentWeakReference.get() != null) {
                mAddInventoryFragmentWeakReference.get().mDeleteInventoryButton
                        .setVisibility(View.VISIBLE);
                mAddInventoryFragmentWeakReference.get().mProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void showRetry() {
            // No-op
        }

        @Override
        public void hideRetry() {
            // No-op
        }

        @Override
        public void showError(String message) {
            if (mAddInventoryFragmentWeakReference.get() != null) {
                mAddInventoryFragmentWeakReference.get().showToast(message);
            }
        }

        @Override
        public Context getAppContext() {
            if (mAddInventoryFragmentWeakReference.get() != null) {
                return mAddInventoryFragmentWeakReference.get().getContext();
            }
            return null;
        }
    }
}
