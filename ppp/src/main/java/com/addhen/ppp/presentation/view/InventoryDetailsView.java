package com.addhen.ppp.presentation.view;

import com.addhen.android.raiburari.presentation.ui.view.LoadDataView;
import com.addhen.ppp.presentation.model.InventoryModel;

/**
 * @author InventoryDetailsView
 */
public interface InventoryDetailsView extends LoadDataView {

    /**
     * Render an Inventory in the UI
     *
     * @param inventoryModel The {@link InventoryModel} to be shown
     */
    void showInventoryDetails(InventoryModel inventoryModel);
}
