package com.addhen.ppp;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.runner.RunWith;

import android.support.annotation.StringRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static org.hamcrest.core.Is.is;

/**
 * @author Henry Addo
 */
@RunWith(AndroidJUnit4.class)
@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class BaseInstrumentationTestCase {

    protected BaseInstrumentationTestCase() {
        //No-op
    }

    protected ViewInteraction matchToolbarTitle(
            CharSequence title) {
        return onView(isAssignableFrom(Toolbar.class))
                .check(matches(withToolbarTitle(is(title))));
    }

    protected ViewInteraction matchToolbarTitle(@StringRes int resId) {
        CharSequence title = InstrumentationRegistry.getTargetContext().getString(resId);
        return matchToolbarTitle(title);
    }

    private Matcher<Object> withToolbarTitle(final Matcher<CharSequence> textMatcher) {
        return new BoundedMatcher<Object, Toolbar>(Toolbar.class) {
            @Override
            public boolean matchesSafely(Toolbar toolbar) {
                return textMatcher.matches(toolbar.getTitle());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with toolbar title: ");
                textMatcher.describeTo(description);
            }
        };
    }
}
