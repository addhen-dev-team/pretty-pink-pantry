[![wercker status](https://app.wercker.com/status/086539db568b7acccc7416858382747e/s "wercker status")](https://app.wercker.com/project/bykey/086539db568b7acccc7416858382747e)

Pink Pantry 
===========

Is an Android app built on [Raiburari](https://bitbucket.org/eyedol/raiburari) to serve as a sample app for people to learn how to use
Raiburari. In itself, it's a simple inventory management app.

[ ![Landscape mode](https://bytebucket.org/eyedol/inbentori/raw/21a908908f376d853af11eb9db3e88223359215f/screenshots/landscape.png) ](https://bitbucket.org/eyedol/inbentori/src/21a908908f376d853af11eb9db3e88223359215f/screenshots/?at=master)

[ ![Inventory listing](https://bytebucket.org/eyedol/inbentori/raw/21a908908f376d853af11eb9db3e88223359215f/screenshots/home.png) ](https://bitbucket.org/eyedol/inbentori/src/21a908908f376d853af11eb9db3e88223359215f/screenshots/?at=master) [ ![Show Navigation drawer](https://bytebucket.org/eyedol/inbentori/raw/21a908908f376d853af11eb9db3e88223359215f/screenshots/navdrawer.png) ](https://bitbucket.org/eyedol/inbentori/src/21a908908f376d853af11eb9db3e88223359215f/screenshots/?at=master) [ ![Update Inventory](https://bytebucket.org/eyedol/inbentori/raw/21a908908f376d853af11eb9db3e88223359215f/screenshots/update.png) ](https://bitbucket.org/eyedol/inbentori/src/21a908908f376d853af11eb9db3e88223359215f/screenshots/?at=master)


Build
=====
`./gradlew build`

License
--------

    Copyright 2015 Henry Addo

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
